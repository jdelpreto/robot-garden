
#ifndef FLOWER_CONTROL_MEGA_H
#define FLOWER_CONTROL_MEGA_H

#include "led_strip.h"
#include "string_functions.h"
#include "OneWireSerial.h"

#define NUM_FLOWERS 8
#define LED_STRIPS_PER_FLOWER 3
#define LEDS_PER_STRIP 10
#define VALVES_PER_FLOWER 1
int valveTiming[NUM_FLOWERS / 2] = {3500, 3500, 3500, 3500}; // Max time, in ms, that each valve is allowed to stay on

boolean valveInflating[NUM_FLOWERS][VALVES_PER_FLOWER];          // Whether each valve is set to closed or open
unsigned long valveInflateTime[NUM_FLOWERS][VALVES_PER_FLOWER];// Time at which each valve was last closed
PololuLedStripBase* strips[NUM_FLOWERS][LED_STRIPS_PER_FLOWER];     // Array of controllers for RGB LEDs
rgb_color ledColors[NUM_FLOWERS][LED_STRIPS_PER_FLOWER][LEDS_PER_STRIP];

// Variables for Bluetooth communication
char bluetoothData[100]; // Raw data received
int bluetoothDataLength; // Length of data in above array
#define NUM_ARGS 10      // Max number of arguments a command can have (number of <>)
#define NUM_SUBARGS 3    // Max number of subarguments a single argument can have (separated by '-' within an argument)
char command[20];                // The received command
int args[NUM_ARGS][NUM_SUBARGS]; // The received arguments (an entry is -1 if no argument)
int numArgs = 0;                 // The number of received arguments
#define numSubArgs(argNum) (args[argNum][0] == -1 ? 0 : args[argNum][1] == -1 ? 1 : args[argNum][2] == -1 ? 2 : 3) // Calculate number of subarguments in an argument

// Variables for hardware mode switches
int modeSwitchMasterPin = 0;
int modeSwitchPins[] = {};
int modeSwitchesCount = sizeof(modeSwitchPins)/sizeof(int);

bool autoMode = true;    // Autonomous mode (will exit this mode whenever a BT command is received that is not AUTO
bool flood = false;
int tileNumber = -1;
char tileNumberHolder[4];

#define NUM_SERIAL 4
#define LEFT 0
#define TOP 1
#define RIGHT 2
#define BOTTOM 3
//int serialRXPins[NUM_SERIAL] = {50, 51, 52, 53}; // Left, Top, Right, Bottom (or whatever ordering you want to use)
//int serialTXPins[NUM_SERIAL] = {A8, A9, A10, A11}; // Left, Top, Right, Bottom (or whatever ordering you want to use)

int serialRXPins[NUM_SERIAL] = {50, 51, A10, A11}; // Left, Top, Right, Bottom (or whatever ordering you want to use)
int serialTXPins[NUM_SERIAL] = {A8, A9, 52, 53}; // Left, Top, Right, Bottom (or whatever ordering you want to use)

bool serialPresent[NUM_SERIAL];
OneWireSerial serial[NUM_SERIAL]; // array of OneWireSerial objects
char serialData[50];       // data received from a serial port
// Probably will make sense to make this an array of received data, one entry for each serial port
int serialDataLength = 0;
int serialSender = -1;
int commandStartIndex = 0;

int tileAddress = -1;

int serialMessagesSent = 0;
int serialMessagesReceived = 0;
int btMessagesSent = 0;
int btMessagesReceived = 0;
int commandsProcessed = 0;
unsigned long sendTime = 0;
unsigned long receiveTime = 0;


// Graph coloring
#define TL 4
#define TR 5
#define BR 6
#define BL 7
const int numColorNeighbors = 8;
int colors[numColorNeighbors];
int color;
int coloringInitialized = false;
int colorDelay = 0;
void initColoring();
void processColoring(int sender, int colors[3]);
void updateColors(int sender, int colors[3]);
void chooseColor();
void sendColors(int dest);
void sendColors(int dest, int color0, int color1, int color2, int color3, int color4);

//================================================
// Helper functions defined below
//================================================
void setupFlowerControl();
void autoAddress();
void setAddress(int address);
void createStripControllers(PololuLedStripBase* strips[NUM_FLOWERS][LED_STRIPS_PER_FLOWER]);

void setColor(int flower, byte red, byte green, byte blue);
void setColor(byte red, byte green, byte blue);
void setColor(int flower, int strip, byte red, byte green, byte blue);
void setColor(int flower, int strip, int led, byte red, byte green, byte blue);
void sendLEDcolors(int flower);
void sendLEDcolors(int flower, int strip);
void deflate(int flower);
void inflate(int flower);
void deflate(int flower, int valve);
void inflate(int flower, int valve);
void controlValves();

void processCOM();
void processBluetooth();
bool getBluetoothData();
void sendBluetoothData(char* data, int len);
void sendBluetoothData(const char* data);
void processSerial();
bool getSerialData(int serialNum);
void sendSerialData(int serialNum, const char* message);
bool parseData(char* data, int dataLength);
bool processCommand();
void formatData(char* commandIn, int argsIn[][NUM_SUBARGS], int numArgsIn, int numSubArgsIn, char* dest, int destLength);

void debugPrint(const char* message);
void debugPrintln(const char* message);
void debugPrint(double message);
void debugPrintln(double message);
void debugPrint(int message);
void debugPrintln(int message);
void debugPrint(unsigned long message);
void debugPrintln(unsigned long message);
void debugPrint(char message);
void debugPrintln(char message);
void debugPrintln();

//================================================
// Pins provided for the flowers and valves
//================================================
const byte flowerPins[][3] =
{
  {25, 24, A0},
  {26, 27, A1},
  {28, 29, A2},
  {30, 31, A3},
  {32, 33, A4},
  {34, 35, A5},
  {36, 37, A6},
  {38, 39, A7}
  //{40, 41, A8}
};

const byte valvePins[][1] =
{
  {10},//, 9, 8},
  {7},//, 6, 5},
  {4},//, 3, 2},
  {16},//, 17, 18}
  // now wrap around since each valve controls 2 flowers
  {10},//, 9, 8},
  {7},//, 6, 5},
  {4},//, 3, 2},
  {16}//, 17, 18}  
  //{19, 23, 22},
  //{53, 52, 50},
  //{51, 48, 49},
  //{44, 47, 46},
  //{45, 42, 43}
};

#define FLOWER_0_PIN_0  25
#define FLOWER_0_PIN_1  24
#define FLOWER_0_PIN_2  A0
#define FLOWER_1_PIN_0  26
#define FLOWER_1_PIN_1  27
#define FLOWER_1_PIN_2  A1
#define FLOWER_2_PIN_0  28
#define FLOWER_2_PIN_1  29
#define FLOWER_2_PIN_2  A2
#define FLOWER_3_PIN_0  30
#define FLOWER_3_PIN_1  31
#define FLOWER_3_PIN_2  A3
#define FLOWER_4_PIN_0  32
#define FLOWER_4_PIN_1  33
#define FLOWER_4_PIN_2  A4
#define FLOWER_5_PIN_0  34
#define FLOWER_5_PIN_1  35
#define FLOWER_5_PIN_2  A5
#define FLOWER_6_PIN_0  36
#define FLOWER_6_PIN_1  37
#define FLOWER_6_PIN_2  A6
#define FLOWER_7_PIN_0  38
#define FLOWER_7_PIN_1  39
#define FLOWER_7_PIN_2  A7

#define VALVE_0_PIN_0  10
#define VALVE_1_PIN_0  7
#define VALVE_2_PIN_0  4
#define VALVE_3_PIN_0  16

#define flowerPin(flower, pin) FLOWER_ ## flower ## _PIN_ ## pin
#define valvePin(flower, pin) VALVE_ ## flower ## _PIN_ ## pin

//================================================
// Setup functions
//================================================

//----------------------
// Start serial if debugging is turned on
// Set up pin modes
// Start interrupt timer for controlling valves
//----------------------
void setupFlowerControl()
{
#ifdef DEBUG
  Serial.begin(9600);
#endif
  debugPrint("Starting setup.... ");
  createStripControllers(strips);
  debugPrint("\n\tSetting up pin modes...");
  for (int flower = 0; flower < NUM_FLOWERS; flower++)
    for (int strip = 0; strip < LED_STRIPS_PER_FLOWER; strip++)
      pinMode(flowerPins[flower][strip], OUTPUT);
  for (int flower = 0; flower < NUM_FLOWERS; flower++)
    for (int valve = 0; valve < VALVES_PER_FLOWER; valve++)
      pinMode(valvePins[flower][valve], OUTPUT);
  debugPrint(" complete");

  debugPrint("\n\tSetting up interrupt timer...");
  debugPrint(" complete");
  //debugPrint(" skipped");

  debugPrint("\n\tSetting up bluetooth...");
  Serial3.begin(9600);
  //sendBluetoothData("AT+NAME");
  //sendBluetoothData("gardenTile");
  //getBluetoothData();
  //debugPrint(" ... skipped");
  debugPrint("\t\t... complete");

  // Initialize the serial ports
  debugPrint("\n\tSetting up serial pins...");
  for (int i = 0; i < NUM_SERIAL; i++)
  {
    serial[i].setPins(serialRXPins[i], serialTXPins[i]);
    serial[i].begin(9600);
    serial[i].flush();
  }
  
  // Initialize the mode switch pins
  debugPrint("\n\tSetting up mode switch pins...");
  pinMode(modeSwitchMasterPin, INPUT_PULLUP);
  for(int i = 0; i < modeSwitchesCount; i++)
    pinMode(modeSwitchPins[i], INPUT_PULLUP);

  randomSeed(analogRead(A12));
  debugPrintln(" complete");
}

void autoAddress()
{
  if (tileAddress < 0)
  {
    static long pingTime = 0;
    static unsigned long lastAddressRequestTime = 0;
    // See if I am tile 0
    if (serialPresent[RIGHT] && serialPresent[BOTTOM] && !serialPresent[LEFT] && !serialPresent[TOP])
    {
      setAddress(0);
    }
    else if (millis() - lastAddressRequestTime > 10000)
    {
      Serial.println("Requesting addresses");
      for (int i = 0; i < NUM_SERIAL; i++)
      {
        sendSerialData(i, "ADDR?");
      }
      lastAddressRequestTime = millis();
    }
  }
}

void setAddress(int address)
{
  if (tileAddress < 0)
  {
    tileAddress = address;
    Serial.print("Figured out my address at time "); Serial.println(millis());

    char myAddress[4];
    itoa(tileAddress, myAddress, 10);
    char toSend[12];
    toSend[0] = '\0';
    concat(toSend, "ADDR<", toSend, 10);
    concat(toSend, myAddress, toSend, 10);
    concat(toSend, ">", toSend, 10);

    sendSerialData(RIGHT, toSend);
    sendSerialData(BOTTOM, toSend);

    Serial.print("Serial messages sent/received   : ");
    Serial.print(serialMessagesSent); Serial.print(" / "); Serial.println(serialMessagesReceived);
    Serial.print("Bluetooth messages sent/received: ");
    Serial.print(btMessagesSent); Serial.print(" / "); Serial.println(btMessagesReceived);
    Serial.print("Commands processed: "); Serial.println(commandsProcessed);
    Serial.print("I am tile "); Serial.print(address); Serial.println("!");
    Serial.println("-----------------------------------");
    for (int i = 0; i < tileAddress + 1; i++)
    {
      digitalWrite(13, HIGH);
      delay(100);
      digitalWrite(13, LOW);
      delay(250);
    }
  }
}

//----------------------
// Creates controllers for the RGB LED strips
// Each flower has 3 strips except for flower at index 8 (the ninth one) which only has 2 strips
// Cannot use loop for this since the template parameters must be constant at compile time
// Uses macros to get pins instead (defined above)
//----------------------
void createStripControllers(PololuLedStripBase* strips[NUM_FLOWERS][LED_STRIPS_PER_FLOWER])
{
  debugPrint("\n\tSetting up LED strip controllers...");
  strips[0][0] = new PololuLedStrip<flowerPin(0, 0)>;
  strips[0][1] = new PololuLedStrip<flowerPin(0, 1)>;
  strips[0][2] = new PololuLedStrip<flowerPin(0, 2)>;
  strips[1][0] = new PololuLedStrip<flowerPin(1, 0)>;
  strips[1][1] = new PololuLedStrip<flowerPin(1, 1)>;
  strips[1][2] = new PololuLedStrip<flowerPin(1, 2)>;
  strips[2][0] = new PololuLedStrip<flowerPin(2, 0)>;
  strips[2][1] = new PololuLedStrip<flowerPin(2, 1)>;
  strips[2][2] = new PololuLedStrip<flowerPin(2, 2)>;
  strips[3][0] = new PololuLedStrip<flowerPin(3, 0)>;
  strips[3][1] = new PololuLedStrip<flowerPin(3, 1)>;
  strips[3][2] = new PololuLedStrip<flowerPin(3, 2)>;
  strips[4][0] = new PololuLedStrip<flowerPin(4, 0)>;
  strips[4][1] = new PololuLedStrip<flowerPin(4, 1)>;
  strips[4][2] = new PololuLedStrip<flowerPin(4, 2)>;
  strips[5][0] = new PololuLedStrip<flowerPin(5, 0)>;
  strips[5][1] = new PololuLedStrip<flowerPin(5, 1)>;
  strips[5][2] = new PololuLedStrip<flowerPin(5, 2)>;
  strips[6][0] = new PololuLedStrip<flowerPin(6, 0)>;
  strips[6][1] = new PololuLedStrip<flowerPin(6, 1)>;
  strips[6][2] = new PololuLedStrip<flowerPin(6, 2)>;
  strips[7][0] = new PololuLedStrip<flowerPin(7, 0)>;
  strips[7][1] = new PololuLedStrip<flowerPin(7, 1)>;
  strips[7][2] = new PololuLedStrip<flowerPin(7, 2)>;
  debugPrint(" complete");
}

//================================================
// Actuation functions
//================================================

//----------------------
// Signals that all of the valves on the given flower should deflate
// Actuation/timing takes place in the interrupt routine
//----------------------
void deflate(int flower)
{
  debugPrint("Deflating flower "); debugPrintln(flower);
  for (int valve = 0; valve < VALVES_PER_FLOWER; valve++)
  {
    valveInflating[flower][valve] = false;
    valveInflateTime[flower][valve] = 0;
  }
  controlValves();
}

//----------------------
// Signals that the given valve on the given flower should deflate
// Actuation/timing takes place in the interrupt routine
//----------------------
void deflate(int flower, int valve)
{
  debugPrint("Deflating valve "); debugPrint(flower);
  debugPrint("-"); debugPrintln(valve);
  valveInflating[flower][valve] = false;
  valveInflateTime[flower][valve] = 0;
  controlValves();
}

//----------------------
// Signals that all of the valves on the given flower should inflate
// Actuation/timing takes place in the interrupt routine
//----------------------
void inflate(int flower)
{
  debugPrint("Inflating flower "); debugPrintln(flower);
  int firstFlower = flower / 2 * 2;
  int secondFlower = firstFlower + 1;
  for (int valve = 0; valve < VALVES_PER_FLOWER; valve++)
  {
    valveInflating[firstFlower][valve] = true;
    valveInflating[secondFlower][valve] = true;
  }
  controlValves();
}

//----------------------
// Signals that the given valve on the given flower should inflate
// Actuation/timing takes place in the interrupt routine
//----------------------
void inflate(int flower, int valve)
{
  debugPrint("Inflating valve "); debugPrint(flower);
  debugPrint("-"); debugPrintln(valve);
  int firstFlower = flower / 2 * 2;
  int secondFlower = firstFlower + 1;
  valveInflating[firstFlower][valve] = true;
  valveInflating[secondFlower][valve] = true;
  controlValves();
}

//----------------------
// Controls valves
// Note that each individual flower can be set to inflate/deflate, but actual control is at pair-wise level now
//  Flowers 0 and 1 will be controlled together, 2 and 3, etc.
//  If two flowers in the same pair are controlled differently, the resulting behavior may be a bit unexpected.
//----------------------
void controlValves()
{
  // Update each valve state based on desired state and timing
  for (int flower = 0; flower < NUM_FLOWERS; flower++)
    for (int valve = 0; valve < VALVES_PER_FLOWER; valve++)
    {
      int firstFlower = ((int)(flower / 2)) * 2;
      int secondFlower = firstFlower + 1;
      int flowerValve = (int)(flower / 2);
      // If it should be open, open it and reset its close time
      if (!valveInflating[firstFlower][valve] || !valveInflating[secondFlower][valve])
      {
        digitalWrite(valvePins[flowerValve][valve], LOW);
        valveInflateTime[firstFlower][valve] = 0;
        valveInflateTime[secondFlower][valve] = 0;
      }
      else
      {
        // Update close time to current time if it has not been closed yet
        if (valveInflateTime[firstFlower][valve] == 0 || valveInflateTime[secondFlower][valve] == 0)
        {
          digitalWrite(valvePins[flowerValve][valve], HIGH);
          valveInflateTime[firstFlower][valve] = millis();
          valveInflateTime[secondFlower][valve] = millis();
        }
        // If alloted max time is passed, open it again and mark it as open
        if (millis() - valveInflateTime[firstFlower][valve] > valveTiming[flowerValve] || millis() - valveInflateTime[secondFlower][valve] > valveTiming[flowerValve])
        {
          digitalWrite(valvePins[flowerValve][valve], LOW);
          valveInflating[firstFlower][valve] = false;
          valveInflating[secondFlower][valve] = false;
          valveInflateTime[firstFlower][valve] = 0;
          valveInflateTime[secondFlower][valve] = 0;
        }
      }
    }
}

//================================================
// LED strip functions
//================================================

void setColor(byte red, byte green, byte blue)
{
  for(int i = 0; i < NUM_FLOWERS; i++)
    setColor(i, red, green, blue);
}

//----------------------
// Sets the color of every LED on every strip of the given flower to the given color
// Color values should be between 0 and 255
//----------------------
void setColor(int flower, byte red, byte green, byte blue)
{
  debugPrint("Setting color of flower "); debugPrint(flower);
  debugPrint(" to "); debugPrint(red);
  debugPrint(" | "); debugPrint(green);
  debugPrint(" | "); debugPrint(blue);
  debugPrintln();
  for (int strip = 0; strip < LED_STRIPS_PER_FLOWER; strip++)
  {
    for (int led = 0; led < LEDS_PER_STRIP; led++)
    {
      ledColors[flower][strip][led].green = green;
      ledColors[flower][strip][led].red = red;
      ledColors[flower][strip][led].blue = blue;
    }
  }
  sendLEDcolors(flower);
}

//----------------------
// Sets the color of every LED on the given strip of the given flower to the given color
// Color values should be between 0 and 255
//----------------------
void setColor(int flower, int strip, byte red, byte green, byte blue)
{
  debugPrint("Setting color of strip "); debugPrint(flower);
  debugPrint("-"); debugPrint(strip);
  debugPrint(" to "); debugPrint(red);
  debugPrint(" | "); debugPrint(green);
  debugPrint(" | "); debugPrint(blue);
  debugPrintln();
  for (int led = 0; led < LEDS_PER_STRIP; led++)
  {
    ledColors[flower][strip][led].green = green;
    ledColors[flower][strip][led].red = red;
    ledColors[flower][strip][led].blue = blue;
  }
  sendLEDcolors(flower, strip);
}

//----------------------
// Sets the color of the given LED on the given strip of the given flower to the given color
// Color values should be between 0 and 255
//----------------------
void setColor(int flower, int strip, int led, byte red, byte green, byte blue)
{
  debugPrint("Setting color of led "); debugPrint(flower);
  debugPrint("-"); debugPrint(strip);
  debugPrint("-"); debugPrint(led);
  debugPrint(" to "); debugPrint(red);
  debugPrint(" | "); debugPrint(green);
  debugPrint(" | "); debugPrint(blue);
  debugPrintln();
  ledColors[flower][strip][led].green = green;
  ledColors[flower][strip][led].red = red;
  ledColors[flower][strip][led].blue = blue;
  sendLEDcolors(flower, strip);
}

//----------------------
// Sends the desired colors (specified by the ledColors array which is set by above functions)
// for every strip on the given flower
//----------------------
void sendLEDcolors(int flower)
{
  for (int strip = 0; strip < LED_STRIPS_PER_FLOWER; strip++)
    sendLEDcolors(flower, strip);
}

//----------------------
// Sends the desired colors (specified by the ledColors array which is set by above functions)
// for the given strip on the given flower
//----------------------
void sendLEDcolors(int flower, int strip)
{
  if (strips[flower][strip] == NULL) // In particular, this catches trying to use the third strip on the ninth flower (which doesn't exist)
    return;
  strips[flower][strip]->write(ledColors[flower][strip], LEDS_PER_STRIP);
}

//================================================
// Serial and Bluetooth functions
//================================================

//----------------------
// Reads, parses, and executes COM commands as if they were bluetooth commands
//----------------------
void processCOM()
{
  bool goodCommand = false;
  char comData[50];
  if (!Serial.available())
    return;
  bluetoothDataLength = 0;
  while (Serial.available())
  {
    bluetoothData[bluetoothDataLength++] = Serial.read();
    delay(5);
  }
  bluetoothData[bluetoothDataLength++] = '\0';
  if (parseData(bluetoothData, bluetoothDataLength)) // Parse any bluetooth data into command and arguments
  {
    btMessagesReceived++;
    if (tileNumber == tileAddress || tileNumber == -1)
    {
      if (processCommand()) // Execute command if it is recognized
        goodCommand = true;
    }
    else // Forward command to other tiles
    {
      unsigned long sendTime = 0;
      goodCommand = true;
      // If I know my address, determine good direction to forward
      if (tileAddress >= 0)
      {
        int myRow = tileAddress / 6;
        int otherRow = tileNumber / 6;
        int myCol = tileAddress % 6;
        int otherCol = tileNumber % 6;
        int serialDest = -1;
        int bestRowNeighbor = otherCol > myCol ? RIGHT : LEFT;
        int bestColNeighbor = otherRow > myRow ? BOTTOM : TOP;
        if (myRow == otherRow && serialPresent[bestRowNeighbor])
          serialDest = bestRowNeighbor;
        else if (serialSender != bestColNeighbor && serialPresent[bestColNeighbor])
          serialDest = bestColNeighbor;
        else
          serialDest = bestRowNeighbor;
        sendTime = micros() / 1000;
        sendSerialData(serialDest, bluetoothData);
      }
      else // I don't know my address - forward to everyone (except the sender)
      {
        sendTime = micros() / 1000;
        for (int s = 0; s < NUM_SERIAL; s++)
        {
          if (s != serialSender)
            sendSerialData(s, bluetoothData);
        }
      }
    }
  }
}

//----------------------
// Reads, parses, and executes bluetooth commands
//----------------------
void processBluetooth()
{
  bool goodCommand = false;
  if (getBluetoothData())  // Read any available bluetooth data
  {
    if (parseData(bluetoothData, bluetoothDataLength)) // Parse any bluetooth data into command and arguments
    {
      btMessagesReceived++;
      if (tileNumber == tileAddress || tileNumber == -1)
      {
        if (processCommand()) // Execute command if it is recognized
          goodCommand = true;
      }
      else // Forward command to other tiles
      {
        goodCommand = true;
        // If I know my address, determine good direction to forward
        if (tileAddress >= 0)
        {
          int myRow = tileAddress / 6;   // 0
          int otherRow = tileNumber / 6; // 1
          int myCol = tileAddress % 6;   // 1
          int otherCol = tileNumber % 6; // 4
          int serialDest = -1;
          int bestRowNeighbor = otherCol > myCol ? RIGHT : LEFT; // RIGHT
          int bestColNeighbor = otherRow > myRow ? BOTTOM : TOP; // BOTTOM
          if (myRow == otherRow && serialPresent[bestRowNeighbor])
            serialDest = bestRowNeighbor;
          else if (serialSender != bestColNeighbor && serialPresent[bestColNeighbor])
            serialDest = bestColNeighbor;
          else
            serialDest = bestRowNeighbor;
          sendTime = micros() / 1000;
          sendSerialData(serialDest, bluetoothData);
        }
        else // I don't know my address - forward to everyone (except the sender)
        {
          sendTime = micros() / 1000;
          for (int s = 0; s < NUM_SERIAL; s++)
          {
            if (s != serialSender)
              sendSerialData(s, bluetoothData);
          }
        }
        if (equals(command, "TIME") && serialSender == -1)
        {
          boolean gotReturn = false;
          unsigned long startWait = millis();
          while (!gotReturn && millis() - startWait < 5000)
          {
            for (int i = 0; i < NUM_SERIAL; i++)
            {
              if (getSerialData(i))
              {
                parseData(serialData, serialDataLength);
                if (equals(command, "TIME"))
                {
                  receiveTime = micros() / 1000;
                  gotReturn = true;
                  break;
                }
              }
            }
          }
          if (millis() - startWait > 5000)
            receiveTime = 0;
          char toSend[50]; toSend[0] = '\0';
          char num[10];
          char digit[2];
          num[6] = '\0';
          itoa(sendTime % 10, digit, 10); num[5] = digit[0]; sendTime /= 10;
          itoa(sendTime % 10, digit, 10); num[4] = digit[0]; sendTime /= 10;
          itoa(sendTime % 10, digit, 10); num[3] = digit[0]; sendTime /= 10;
          itoa(sendTime % 10, digit, 10); num[2] = digit[0]; sendTime /= 10;
          itoa(sendTime % 10, digit, 10); num[1] = digit[0]; sendTime /= 10;
          itoa(sendTime % 10, digit, 10); num[0] = digit[0]; sendTime /= 10;
          concat(toSend, "Sent: ", toSend, 50);
          concat(toSend, num, toSend, 50);
          sendBluetoothData(toSend);

          toSend[0] = '\0';
          num[6] = '\0';
          itoa(receiveTime % 10, digit, 10); num[5] = digit[0]; receiveTime /= 10;
          itoa(receiveTime % 10, digit, 10); num[4] = digit[0]; receiveTime /= 10;
          itoa(receiveTime % 10, digit, 10); num[3] = digit[0]; receiveTime /= 10;
          itoa(receiveTime % 10, digit, 10); num[2] = digit[0]; receiveTime /= 10;
          itoa(receiveTime % 10, digit, 10); num[1] = digit[0]; receiveTime /= 10;
          itoa(receiveTime % 10, digit, 10); num[0] = digit[0]; receiveTime /= 10;
          concat(toSend, "Received: ", toSend, 50);
          concat(toSend, num, toSend, 50);
          sendBluetoothData(toSend);
          return;
        }
      }
    }
    if (goodCommand)
      sendBluetoothData("OK");
    else if (bluetoothDataLength > 0)
      sendBluetoothData("BAD");
  }
}

//----------------------
// Reads, parses, and executes serial commands
//----------------------
void processSerial()
{
  bool goodCommand = false;
  for (int i = 0; i < NUM_SERIAL; i++)
  {
    goodCommand = false;
    if (getSerialData(i))  // Read any available serial data
    {
      if (parseData(serialData, serialDataLength)) // Parse any bluetooth data into command and arguments
      {
        serialMessagesReceived++;
        if (tileNumber == tileAddress && equals(command, "TIME"))
        {
          tileNumber = 0;
          serialData[0] = '0';
          serialData[1] = '_';
          serialData[2] = 'T';
          serialData[3] = 'I';
          serialData[4] = 'M';
          serialData[5] = 'E';
          serialData[6] = '\0';
          serialData[7] = '\0';
          serialDataLength = 7;
          serialSender = -1;
        }
        if (tileNumber == tileAddress || tileNumber == -1)
        {
          if (processCommand()) // Execute command if it is recognized
            goodCommand = true;
        }
        else // Forward command to other tiles
        {
          goodCommand = true;
          // If I know my address, determine good direction to forward
          if (tileAddress >= 0)
          {
            int myRow = tileAddress / 6;
            int otherRow = tileNumber / 6;
            int myCol = tileAddress % 6;
            int otherCol = tileNumber % 6;
            int serialDest = -1;
            int bestRowNeighbor = otherCol > myCol ? RIGHT : LEFT;
            int bestColNeighbor = otherRow > myRow ? BOTTOM : TOP;
            if (myRow == otherRow && serialPresent[bestRowNeighbor])
              serialDest = bestRowNeighbor;
            else if (serialSender != bestColNeighbor && serialPresent[bestColNeighbor])
              serialDest = bestColNeighbor;
            else
              serialDest = bestRowNeighbor;
            sendSerialData(serialDest, serialData);
          }
          else // I don't know my address - forward to everyone (except the sender)
          {
            for (int s = 0; s < NUM_SERIAL; s++)
            {
              if (s != serialSender)
                sendSerialData(s, serialData);
            }
          }
        }
      }
      //      if(goodCommand)
      //        sendSerialData("OK");
      //      else if(serialDataLength > 0)
      //        sendSerialData("BAD");
    }
  }
}

//----------------------
// Reads, parses, and executes commands based on physical switches
//----------------------
void processModeSwitches()
{
  // see if master is pressed, indicating command should be read and sent
  // note active low since input_pullup is offered
  if(digitalRead(modeSwitchMasterPin))
    return;
  delay(250); // debounce
  if(digitalRead(modeSwitchMasterPin))
    return;
  // wait for master switch to be released
  while(!digitalRead(modeSwitchMasterPin))
    delay(10);
  delay(250); // debounce
  
  // read switches to determine command
  int modeCommandIndex = 0;
  for(int i = 0; i < modeSwitchesCount; i++)
    if(!digitalRead(modeSwitchPins[i]))
      modeCommandIndex += 1 << i;
      
  // Determine command based on index
  bool goodCommand = false;
  char modeCommand[100];
  modeCommand[0] = '\0';
  switch(modeCommandIndex)
  {
    case 0: strcpy("KIOSK", modeCommand, 100); break;
    case 1: strcpy("BFS<15>", modeCommand, 100); break;
    case 2: strcpy("COLOR", modeCommand, 100); break;
    case 3: strcpy("DFS<15>", modeCommand, 100); break;
    case 4: strcpy("WAVE", modeCommand, 100); break;
    case 5: strcpy("DIST", modeCommand, 100); break;
    case 6: strcpy("BFSRESET", modeCommand, 100); break;    
    case 7: strcpy("MUSIC", modeCommand, 100); break;      
    default: modeCommand[0] = '\0'; break;
  }
  if(length(modeCommand) > 0)
  {
    if(parseData(modeCommand, 100)) // Parse the command into command and arguments
    {
      if(tileNumber == tileAddress || tileNumber == -1)
      {
        if(processCommand()) // Execute command if it is recognized
          goodCommand = true;
      }
      else // Forward command to other tiles
      {
        goodCommand = true;
        // If I know my address, determine good direction to forward
        if(tileAddress >= 0)
        {
          int myRow = tileAddress / 6;   // 0
          int otherRow = tileNumber / 6; // 1
          int myCol = tileAddress % 6;   // 1
          int otherCol = tileNumber % 6; // 4
          int serialDest = -1;
          int bestRowNeighbor = otherCol > myCol ? RIGHT : LEFT; // RIGHT
          int bestColNeighbor = otherRow > myRow ? BOTTOM : TOP; // BOTTOM
          if (myRow == otherRow && serialPresent[bestRowNeighbor])
            serialDest = bestRowNeighbor;
          else if (serialSender != bestColNeighbor && serialPresent[bestColNeighbor])
            serialDest = bestColNeighbor;
          else
            serialDest = bestRowNeighbor;
          sendTime = micros() / 1000;
          sendSerialData(serialDest, bluetoothData);
        }
        else // I don't know my address - forward to everyone (except the sender)
        {
          sendTime = micros() / 1000;
          for (int s = 0; s < NUM_SERIAL; s++)
          {
            if (s != serialSender)
              sendSerialData(s, bluetoothData);
          }
        }
      }
    }
  }
}

//----------------------
// Gets any available bluetooth data
// Times out after 500ms
//----------------------
bool getBluetoothData()
{
  //bt.listen();
  bluetoothDataLength = 0;
  if (!Serial3.available())
    return false;
  unsigned long timeout = 100;
  unsigned long start = millis();
  boolean terminated = false;
  start = millis();
  debugPrint("\nGetting bluetooth data: <");
  while (!terminated && millis() - start < timeout)
  {
    while (!Serial3.available() && millis() - start < timeout);
    if (Serial3.available())
    {
      bluetoothData[bluetoothDataLength++] = (char)Serial3.read();
      terminated = (bluetoothData[bluetoothDataLength - 1] == '\0');
      if (!terminated)
        debugPrint((char)bluetoothData[bluetoothDataLength - 1]);
      delay(1);
      start = millis();
    }
  }
  bluetoothData[bluetoothDataLength++] = '\0';
  debugPrintln(">");
  if (bluetoothDataLength == 3 && bluetoothData[0] == '?')
  {
    sendBluetoothData("?\0", 3);
    bluetoothDataLength = 0;
    debugPrintln("Got heartbeat");
  }
  debugPrint("Got BT data: <"); debugPrint(bluetoothData); debugPrintln(">");
  return true;
}

//----------------------
// Sends data over bluetooth
//----------------------
void sendBluetoothData(char* data, int len)
{
  debugPrint("Sending BT data: <");
  //bt.listen();
  for (int i = 0; i < len; i++)
  {
    Serial3.write(data[i]);
#ifdef DEBUG
    debugPrint((char)data[i]);
#endif
    delay(5);
  }
  if (data[len - 1] != '\0')
  {
    char endChar[1];
    endChar[0] = '\0';
    Serial3.write(endChar[0]);
  }
  debugPrintln(">");
  btMessagesSent++;
}

void sendBluetoothData(const char* data)
{
  sendBluetoothData((char*)data, length(data));
}

// Receive data from the given serial port, if any is available
// Will timeout waiting for data
bool getSerialData(int serialNum)
{
  serialData[0] = '\0'; // clear previously received data
  serialDataLength = 0;
  serialSender = -1;
  int timeout = 50;
  // If no data is available, return
  if (serial[serialNum].available() == 0)
    return false;

  // Read from port until no more data is available
  // Use timeout between characters
  debugPrint("\nReceiving from serial "); debugPrint(serialNum); debugPrint(": <");
  unsigned long start = millis();
  boolean terminated = false;
  while (serial[serialNum].available() > 0 && !terminated && millis() - start < timeout)
  {
    start = millis();
    serialData[serialDataLength++] = (char)serial[serialNum].read();
    terminated = (serialData[serialDataLength - 1] == '\0');
    if (!terminated)
    {
      debugPrint((char)serialData[serialDataLength - 1]);
      while (serial[serialNum].available() == 0 && millis() - start < timeout)
        delay(2);
    }
  }
  if (!terminated)
    serialData[serialDataLength] = '\0'; // null-terminate the character array
  debugPrintln(">");
  serialSender = serialNum;
  serialPresent[serialNum] = true;
  return true;
}

// Send the given character array to the given serial port
void sendSerialData(int serialNum, const char* message)
{
  //serial[serialNum].listen(); // probably not needed for sending, but just in case
  //    debugPrint("Sending to serial "); debugPrint(serialNum); debugPrint(": <");
  // Send one character at a time for now
  // Might be able to use serial[serialNum].print(message) to send it all at once instead, but haven't tried yet
  serial[serialNum].print(message);
  //serial[serialNum].print('\0');
  //    debugPrintln(">");
  Serial.print("Sent <"); Serial.print(message); Serial.print("> to ");
  Serial.print(serialNum); Serial.print(" at time "); Serial.println(millis());
  serialMessagesSent++;
}

void formatData(char* commandIn, int argsIn[][NUM_SUBARGS], int numArgsIn, int numSubArgsIn, char* dest, int destLength)
{
  //char dest[100];
  dest[0] = '\0';
  strcpy(commandIn, dest, destLength);
  for(int i = 0; i < numArgsIn; i++)
  {
    //Serial.print("\ti = "); Serial.println(i);
    concat(dest, "<", dest, destLength);
    for(int j = 0; j < numSubArgsIn; j++)
    {
      //Serial.print("\t\tj = "); Serial.print(j);
      //Serial.print(" -> "); Serial.println(argsIn[i][j]);
      concatInt(dest, argsIn[i][j], dest, destLength);
      if(j < numSubArgsIn-1)
        concat(dest, "-", dest, destLength);
    }
    concat(dest, ">", dest, destLength);
  }
  //Serial.print("\tFormatted data: <"); Serial.print(dest); Serial.println(">");
  //delay(5000);
}

//----------------------
// Performs the received command, if any
// Returns whether the command was recognized
//----------------------
bool stoppedBFS = false;
int bfsSender = -1;
bool sentBFS = false;
int dest = -1;
int distance = 0;
bool bfsDistanceColoring = false;
bool dfsComplete = false;

bool stopWave = false;

bool processCommand()
{
  if (length(command) == 0)
    return false;
  boolean oldAutoMode = autoMode;
  autoMode = false;
  //
  //    AUTO    // Switch to autonomous mode (any other valid command will exit auto mode)
  //
  //    SET_COLOR<R-G-B>                                              // Set all leds on the tile to given color
  //    SET_COLOR<R-G-B><flower0><flower1><flower2>...                // Set all leds on given flowers to given color
  //    SET_COLOR<R-G-B><flower0-led0><flower1-led1><flower2-led2>... // Set given leds on all strips of the given flowers to given color
  //    SET_COLOR<R-G-B><flower0-strip0-led0><flower1-strip1-led1>... // Set given leds on given strips of given flowers to given color
  //
  //    INFLATE                                                       // Inflate all pouches on all flowers
  //    INFLATE<flower0><flower1><flower2>...                         // Inflate all pouches on given flowers
  //    INFLATE<flower0-pouch0><flower1-pouch1><flower2-pouch2>...    // Inflate given pouches on given flowers
  //
  //    DEFLATE                                                       // Deflate all pouches on all flowers
  //    DEFLATE<flower0><flower1><flower2>...                         // Deflate all pouches on given flowers
  //    DEFLATE<flower0-pouch0><flower1-pouch1><flower2-pouch2>...    // Deflate given pouches on given flowers
  //
  //    INFLATE_TIME<time>                                            // Set the inflation time of all flowers (in ms)
  //    INFLATE_TIME<flower0-time0><flower1-time1>...                 // Set the inflation times of flowers (in ms)
  debugPrint("See command "); debugPrint(command); debugPrint(" at time "); debugPrintln(millis());
  commandsProcessed++;
  if(equals(command, "ADDR?"))
  {
    char myAddress[4];
    itoa(tileAddress, myAddress, 10);
    char toSend[10];
    toSend[0] = '\0';
    concat(toSend, "ADDR<", toSend, 10);
    concat(toSend, myAddress, toSend, 10);
    concat(toSend, ">", toSend, 10);
    sendSerialData(serialSender, toSend);
    return true;
  }
  else if (equals(command, "ADDR"))
  {
    if (tileAddress < 0)
    {
      int address = args[0][0];
      if (address >= 0)
      {
        switch (serialSender)
        {
          case LEFT: setAddress(address + 1); break;
          case RIGHT: setAddress(address - 1); break;
          case TOP: setAddress(address + 6); break;
          case BOTTOM: setAddress(address - 6); break;
          case -1: setAddress(address); break;
        }
      }
    }
    return true;
  }
  else if (equals(command, "ping"))
  {
    sendSerialData(serialSender, "hi");
    return true;
  }
  else if (equals(command, "COLOR"))
  {
    if (!coloringInitialized || serialSender == -1)
      initColoring();
    int receivedColors[5];
    if (numArgs == 1)
      colorDelay = args[0][0];
    if (numArgs == 4)
      colorDelay = args[3][0];
    if (numArgs < 3)
    {
      receivedColors[0] = -1;
      receivedColors[1] = -1;
      receivedColors[2] = -1;
      receivedColors[3] = -1;
      receivedColors[4] = -1;
    }
    else
    {
      receivedColors[0] = args[0][0];
      receivedColors[1] = args[0][1];
      receivedColors[2] = args[1][0];
      receivedColors[3] = args[2][0];
      receivedColors[4] = args[2][1];
    }
    processColoring(serialSender, receivedColors);
    return true;
  }
  else if(equals(command, "WAVESTOP"))
  {
    int waveDir = args[0][0];
    // send wavestop to direction perpendicular to waveDir
    char toSend[50];
    if(serialSender == -1)
      substring(bluetoothData, commandStartIndex, bluetoothDataLength, toSend);
    else
      substring(serialData, commandStartIndex, serialDataLength, toSend);
    //Serial.print("\ttoSend: "); Serial.println(toSend);
    switch(waveDir)
    {
      case TOP:
      case BOTTOM: Serial.println("\tSending to Right"); sendSerialData(RIGHT, toSend); break;
      case RIGHT:
      case LEFT: Serial.println("\tSending to Bottom"); sendSerialData(BOTTOM, toSend); break;
    }
    if(serialSender == -1)
      stopWave = true;
    return true;
  }
  else if(equals(command, "WAVE"))
  {
    static int waveDir = -1;
    static int defaulWaveWait = 1000;
    Serial.println("--------------------");
    Serial.print("waveDir: "); Serial.println(waveDir);
    Serial.print("args[0][0]: "); Serial.println(args[0][0]);    

    if(stopWave)
    {
      waveDir = -1;
      stopWave = false;
    }
    else if(true)//waveDir != args[0][0])
    {
      waveDir = args[0][0];
      Serial.print("Wave dir: "); Serial.println(waveDir);
      // send wave to direction perpendicular to waveDir
      char toSend[50];
      if(serialSender == -1)
        substring(bluetoothData, commandStartIndex, bluetoothDataLength, toSend);
      else
        substring(serialData, commandStartIndex, serialDataLength, toSend);
      //Serial.print("\ttoSend: "); Serial.println(toSend);
      switch(waveDir)
      {
        case TOP:
        case BOTTOM: Serial.println("\tSending to Right"); sendSerialData(RIGHT, toSend); break;
        case RIGHT:
        case LEFT: Serial.println("\tSending to Bottom"); sendSerialData(BOTTOM, toSend); break;
      }
      // light up
      setColor(0, 100, 0);
      // wait
      int waveWait = numArgs > 1 ? args[1][0] : defaulWaveWait;
      Serial.print("\tWave wait: "); Serial.println(waveWait);
      delay(waveWait);
      // reverse wave direction if obstacle
      switch(waveDir)
      {
        case RIGHT: waveDir = serialPresent[RIGHT] ? RIGHT : LEFT; break;
        case LEFT: waveDir = serialPresent[LEFT] ? LEFT : RIGHT; break;
        case TOP: waveDir = serialPresent[TOP] ? TOP : BOTTOM; break;
        case BOTTOM: waveDir = serialPresent[BOTTOM] ? BOTTOM : TOP; break;
      }
      // forward command in wave direction if we should
      // TODO make this not so configuration-specific while still avoiding simultaneous message arrivals
      boolean shouldSend = false;
      if(waveDir == RIGHT || waveDir == LEFT)
        shouldSend = (!serialPresent[TOP] && !(tileAddress == 15 and waveDir == RIGHT) && !(tileAddress == 14 and waveDir == LEFT))
                      || (tileAddress == 13 && waveDir == RIGHT) || (tileAddress == 16 && waveDir == LEFT);
      if(waveDir == TOP || waveDir == BOTTOM)
        shouldSend = (!serialPresent[LEFT] && tileAddress != 10) || tileAddress == 4 || tileAddress == 16;
      if(shouldSend)
      {
        //Serial.println("\tPropogating");
        //Serial.print("numArgs: "); Serial.println(numArgs);
        //Serial.print("args[0][0]: "); Serial.println(args[0][0]);
        int toSendArgs[numArgs][NUM_SUBARGS];
        if(numArgs > 1)
        {
          toSendArgs[1][0] = args[1][0];
          //Serial.print("args[1][0]: "); Serial.println(args[1][0]);
        }
        toSendArgs[0][0] = waveDir;
        formatData(command, toSendArgs, numArgs, 1, toSend, 50);
        sendSerialData(waveDir, toSend);
      }
      // change color
      setColor(0, 0, 100);    
    }
    return true;
  }
  else if(equals(command, "MUSIC"))
  {
    // Get my threshold
    int threshMin = 250;
    int threshRange = 200;
    if(numArgs >= 1)
      threshMin = args[0][0];
    if(numArgs >= 2)
      threshRange = args[1][0];
    // Forward command to put everyone in music mode
    // Set people in my column to my threshold
    // TODO determine sending in more general way
    char toSend[50]; 
    boolean shouldSendDown = (!serialPresent[LEFT] && tileAddress != 10) || tileAddress == 4;
    if(shouldSendDown)
    {
      toSend[0] = '\0';
      concat(toSend, "MUSIC<", toSend, 50);
      concatInt(toSend, threshMin, toSend, 50);
      concat(toSend, "><", toSend, 50);
      concatInt(toSend, threshRange, toSend, 50);
      concat(toSend, ">", toSend, 50);
      sendSerialData(BOTTOM, toSend);
    }
    // Set people in my row to a higher threshold
    toSend[0] = '\0';
    concat(toSend, "MUSIC<", toSend, 50);
    concatInt(toSend, threshMin+threshRange/2, toSend, 50);
    concat(toSend, "><", toSend, 50);
    concatInt(toSend, threshRange, toSend, 50);
    concat(toSend, ">", toSend, 50);
    sendSerialData(RIGHT, toSend);
    // Wait for everyone to be in music mode
    if(tileAddress == 0)
      setColor(0, 0, 0);
    delay(5000);
    // Clear any serial data that is in the buffers, just in case
    while(getSerialData(TOP));
    while(getSerialData(BOTTOM));
    while(getSerialData(LEFT));
    while(getSerialData(RIGHT));
    // Music mode! (listen for bytes directly to be faster)
    setColor(0, 0, 0);
    bool terminate = false;
    int val = threshMin;
    double colorVal = 0;
    Serial.print("ThreshMin: "); Serial.println(threshMin);
    Serial.print("ThreshRange: "); Serial.println(threshRange);
    if(tileAddress == 0)
    {
      while(!terminate)
      {
        val = analogRead(A15);
        //val += 50;
        //val = (val == threshMin) ? 1000 : threshMin;
        //if(val > 1000)
        //  val = threshMin;
        val = val > 0 ? val : 1;
        Serial.print("val: "); Serial.println(val);
        delay(35);
        // Send val to others
        serial[BOTTOM].print(val);
        serial[RIGHT].print(val);
        // Determine my color
        colorVal = double(val - threshMin);
        colorVal = colorVal >= 0 ? colorVal : 0;
        colorVal = (colorVal / (double)threshRange) * 150.0;
        colorVal = colorVal >= 200 ? 200 : (int)colorVal;
        setColor(colorVal, colorVal, colorVal);
        // See if we should terminate
        while(Serial.available())
        {
          Serial.read();
          terminate = true;
        }
        while(Serial3.available())
        {
          Serial3.read();
          terminate = true;
        }
        if(!digitalRead(modeSwitchMasterPin))
          terminate = true;
      }
      // Stop everyone else
      serial[BOTTOM].print(-123);
      serial[RIGHT].print(-123);
      delay(100);
      serial[BOTTOM].print(-123);
      serial[RIGHT].print(-123);
      setColor(0,0,0);
    }
    else
    {
      char valStr[6]; valStr[0] = '\0';
      int index = 0;
      int val = 0;
      while(!terminate)
      {
        index = 0;
        while(!serial[LEFT].available() && !serial[TOP].available());
        Serial.print("Reading val from left: <");
        while(serial[LEFT].available())
        {
          valStr[index++] = serial[LEFT].read();
          Serial.print(valStr[index-1]);
        }
        Serial.println(">");
        if(serial[TOP].available())
        {
          index = 0;
          Serial.print("Reading val from top: <");
          while(serial[TOP].available())
          {
            valStr[index++] = serial[TOP].read();
            Serial.print(valStr[index-1]);
          }
          Serial.println(">");
        }
        valStr[index] = '\0';
        char *end;
        long newVal = strtol(valStr, &end, 10); 
        if(*end != '\0' || newVal == 0)
        {
          Serial.println("Error converting newVal");
        }
        else
        {
          val = (int)newVal;
          Serial.print("Got val: "); Serial.println(val);
          terminate = (val == -123);
          // Send val to others
          serial[RIGHT].print(val);
          if(shouldSendDown)
            serial[BOTTOM].print(val);
          if(terminate)
          {
            delay(100);
            serial[RIGHT].print(val);
            if(shouldSendDown)
              serial[BOTTOM].print(val);
          }
          // Determine my color
          val = val > 0 ? val : 0;
          // Determine my color
          colorVal = double(val - threshMin);
          colorVal = colorVal >= 0 ? colorVal : 0;
          colorVal = (colorVal / (double)threshRange) * 150.0;
          colorVal = colorVal >= 200 ? 200 : (int)colorVal;
          setColor(colorVal, colorVal, colorVal);
        }
      }
      setColor(0,0,0);
    }
  }
  else if(equals(command, "DFS"))
  {
    // Light up red if not goal else light up green
    // delay
    // if goal, send positive to sender
    // Try right if present
    // Wait for response
    //   if positive, light up green, send positive to sender, and end
    // Try down if present
    // wait for response
    //   if positive, light up green, send positive to sender, and end
    // turn leds off
    // send negative to sender
    int parent = (numArgs == 1 ? -1 : serialSender);
    int distance = (numArgs >= 2 ? args[1][0] : 0);
    int goal = args[0][0];
    int delayTime = 500;
    if(numArgs == 3)
      delayTime = args[2][0];
    if(numArgs == 1 && numSubArgs(0) == 2)
      delayTime = args[0][1];
      
    char toSend[50]; toSend[0] = '\0';
    strcpy(serialSender == -1 ? bluetoothData : serialData, toSend, 50);
    if(indexOf(toSend, "_") > 0 && indexOf(toSend, "_") < indexOf(toSend, "DFS"))
      substring(toSend, indexOf(toSend, "_")+1, length(toSend), toSend, 50);
    if(parent == -1)
    {
      Serial.println("Initializing DFS");
      concat(toSend, "<", toSend, 50);
      concatInt(toSend, distance+1, toSend, 50);
      concat(toSend, "><", toSend, 50);
      concatInt(toSend, delayTime, toSend, 50);
      concat(toSend, ">", toSend, 50);
    }
    if(tileAddress == goal)
    {
      Serial.println("I am the goal!");
      setColor(150, 0, 0);
      delay(delayTime);
      concat(toSend, "<1>", toSend, 50);
      if(parent >= 0)
        sendSerialData(parent, toSend);
      return true;
    }
    else if(!dfsComplete)
    {
      setColor(0, 150, 0);
      delay(delayTime);
    }
    if(serialPresent[RIGHT] && !dfsComplete)
    {
      Serial.println("Sending to right");
      sendSerialData(RIGHT, toSend);
      while(!getSerialData(RIGHT)) delay(50);
      parseData(serialData, serialDataLength);
      int foundGoal = args[numArgs-1][0];
      if(foundGoal)
      {
        setColor(150, 0, 0);
        if(parent >= 0)
          sendSerialData(parent, serialData);
        return true;
      }
    }
    if(serialPresent[BOTTOM] && !dfsComplete)
    {
      Serial.println("Sending to bottom");
      sendSerialData(BOTTOM, toSend);
      while(!getSerialData(BOTTOM)) delay(50);
      parseData(serialData, serialDataLength);
      int foundGoal = args[numArgs-1][0];
      if(foundGoal)
      {
        setColor(150, 0, 0);
        if(parent >= 0)
          sendSerialData(parent, serialData);
        return true;
      }
    }
    Serial.println("no success, returning to parent");
    setColor(0, 0, 0);
    if(!dfsComplete)
      delay(delayTime);
    concat(toSend, "<0>", toSend, 50);
    if(parent >= 0)
      sendSerialData(parent, toSend);
    dfsComplete = true;
    return true;
  }
//  else if(equals(command, "BFS"))
//  {
//    static bool dest = -1;
//    static int distance = 0;
//    int newDest = args[0][0];
//    int newDistance = (serialSender >= 0 ? args[1][0] : 0);
//    int delayTime = 500;
//    if(serialSender == -1 && numArgs == 2)
//      delayTime = args[1][0];        
//    else if(numArgs == 3)
//      delayTime = args[2][0];
//      
//    if(dest != newDest)
//    {
//      dest = newDest;
//      distance = newDistance;
//    }
//    else if(newDistance >= distance)
//      return;
//    
//    setColor(0, 100, 0);
//    delay(delayTime);
//    
//    char toSend[50]; toSend[0] = '\0';
//    concat(toSend, "BFS<", toSend, 50);
//    concatInt(toSend, dest, toSend, 50);
//    concat(toSend, "><", toSend, 50);
//    concatInt(toSend, distance+1, toSend, 50);
//    concat(toSend, "><", toSend, 50);
//    concatInt(toSend, delayTime, toSend, 50);
//    concat(toSend, ">", toSend, 50);
//    sendSerialData(BOTTOM, toSend);
//    sendSerialData(RIGHT, toSend);    
//  }
  else if(equals(command, "DFSRESET"))
  {
    dfsComplete = false;
    setColor(0, 0, 150);
    return true;
  }
  else if(equals(command, "BFSFOUND"))
  {
    setColor(100, 0, 0);
    if(bfsSender != -1)
      sendSerialData(bfsSender, "BFSFOUND");
    return true;
  }
  else if(equals(command, "BFSSTOP"))
  {
    if(!stoppedBFS)
    {
      for(int i = 0; i < NUM_SERIAL; i++)
        sendSerialData(i, "BFSSTOP");
      stoppedBFS = true;
    }
    return true;
  }
  else if(equals(command, "BFS") || equals(command, "DIST"))
  {
    // if dest is previous dest and sentBFS or distance arg >= distance
    //   send ack
    //   end
    // if dest is not previous dest and no distance arg
    //   set distance to 0
    //   store dest
    //   clear sentBFS
    // if distance args is provided
    //   set distance to arg
    //   store dest
    //   clear sentBFS
    //   send ack
    //   end
    // else
    //   send DIST<dest><distance+1> to bottom and right neighbors 
    //   set sentBFS
    //   wait for acks
    // send DIST<dest> to bottom and right neighbors      
    bfsDistanceColoring = equals(command, "DIST");
    int newDest = numArgs >= 1 ? args[0][0] : 50;
    int newDistance = (numArgs == 2 ? args[1][0] : -1);
    bool haveDistance = (newDistance >= 0);
    int delayTime = 500;
    if(numArgs >= 1 && numSubArgs(0) == 2)
      delayTime = args[0][1];    
    
    // If BFS has already terminated for this destination, do nothing
    if(stoppedBFS && dest == newDest)
      return true;
        
    // If I am the goal, light up green and let others know to stop
    if(newDest == tileAddress)
    {
      Serial.println("I am the goal!");
      setColor(150, 0, 0);
      if(dest == newDest)
        return true;
      for(int i = 0; i < NUM_SERIAL; i++)
      {
        sendSerialData(i, "BFSSTOP");
        delay(random(250));
      }
      delay(3000);
      if(serialSender >= 0)
        sendSerialData(serialSender, "BFSFOUND");
      dest = newDest;
      stoppedBFS = true;
      return true;
    }
    // If already processed BFS for this goal, send ack but do nothing
    if(dest == newDest && (sentBFS || newDistance >= distance) && serialSender >= 0)
    {
      Serial.println("Ignoring duplicate");
      if(haveDistance)
        sendSerialData(serialSender, "ok");
      return true;
    }
    // If starting new BFS, intialize variables
    if(dest != newDest && !haveDistance)
    {
      Serial.println("initializing");
      distance = 0;
      dest = newDest;
      sentBFS = false;
      stoppedBFS = false;
      if(bfsDistanceColoring)
        setColor(0, 106, 127); //setColor(0, 212, 255); // violet
      else
        setColor(0, 150, 0);
    }    
    // If being told what distance I am, record it, send ack, and exit
    if(haveDistance)
    {
      Serial.println("setting distance");
      distance = newDistance;
      dest = newDest;
      sentBFS = false;
      delay(random(250));
      sendSerialData(serialSender, "ok");
      bfsSender = serialSender;
      if(bfsDistanceColoring)
      {
        // choose color based on distance 
        switch(distance)
        {
          case 1: setColor(0, 0, 127); break;   //setColor(0, 0, 255);   // dark blue
          case 2: setColor(121, 0, 127); break; //setColor(242, 0, 255); // light blue
          case 3: setColor(107, 21, 43); break; //setColor(214, 43, 86); // dark green
          case 4: setColor(127, 61, 0); break;  //setColor(255, 123, 0); // bright green
          case 5: setColor(127, 127, 0); break; //setColor(255, 255, 0); // yellow
          case 6: setColor(83, 127, 0); break;  //setColor(166, 255, 0); // orange
          case 7: setColor(0, 127, 0); break;   //setColor(0, 255, 0);   // red
        }
      }
      else
        setColor(0, 100, 0);
      return true;
    }
    // If being told to continue, send first stage to neighbors
    else
    {
      Serial.println("sending to neighbors");
      delay(delayTime);
      char toSend[50]; toSend[0] = '\0';
      if(!bfsDistanceColoring)
        concat(toSend, "BFS<", toSend, 50);
      else
        concat(toSend, "DIST<", toSend, 50);
      concatInt(toSend, dest, toSend, 50);
      concat(toSend, "-", toSend, 50);
      concatInt(toSend, delayTime, toSend, 50);
      concat(toSend, "><", toSend, 50);
      concatInt(toSend, distance+1, toSend, 50);
      concat(toSend, ">", toSend, 50);
      int ackCount = 0;
      bool acks[4];
      for(int i = 0; i < NUM_SERIAL; i++)
      {
        if(i != bfsSender && serialPresent[i])
        {
          sendSerialData(i, toSend);
          ackCount++;
          delay(random(250));
        }
      }
      sentBFS = true;
      // Wait for acknowledgments of first stage
      // While waiting, check for Stop command in case goal is found elsewhere
      unsigned long waitTime = millis();
      while(ackCount > 0)
      {
        // If I don't hear acks within a timeout period, assume communication failure and resend
        if(millis() - waitTime > 1000)
        {
          for(int i = 0; i < NUM_SERIAL; i++)
          {
            if(i != bfsSender && serialPresent[i] && !acks[i])
              sendSerialData(i, toSend);
            delay(random(250));
          }
          waitTime = millis();     
        }
        // Check for responses
        for(int i = 0; i < NUM_SERIAL; i++)
        {
          if(getSerialData(i))
          {
            // Record acks
            if(i != bfsSender)
            {
              Serial.print("got an ack from "); Serial.println(i);
              ackCount--;
              acks[i] = true;
            }
            // If it was a stop command, exit
            parseData(serialData, serialDataLength);
            if(equals(command, "BFSSTOP"))
            {
              Serial.println("Got STOP");
              for(int i = 0; i < NUM_SERIAL; i++)
              {
                sendSerialData(i, "BFSSTOP");
                delay(random(250));
              }
              stoppedBFS = true;
              return true;
            }
          }
        }
      } // received all acks
    }
    // Send second stage to neighbors
    Serial.println("Sending continue command");
    char toSend[50]; toSend[0] = '\0';
    if(!bfsDistanceColoring)
      concat(toSend, "BFS<", toSend, 50);
    else
      concat(toSend, "DIST<", toSend, 50);
    concatInt(toSend, dest, toSend, 50);
    concat(toSend, ">", toSend, 50);
    for(int i = 0; i < NUM_SERIAL; i++)
    {
      if(i != bfsSender && serialPresent[i])
        sendSerialData(i, toSend);
      delay(random(250));
    }
    return true;
  }
  else if(equals(command, "BFSRESET"))
  {
    stoppedBFS = false;
    bfsSender = -1;
    sentBFS = false;
    dest = -1;
    distance = 0;
    bfsDistanceColoring = false;
    setColor(0, 0, 150);
    return true;
  }
  else if (equals(command, "TIME"))
  {
    sendBluetoothData("OK");
    return true;
  }
  else if (equals(command, "AUTO"))
  {
    autoMode = true;
    return true;
  }
  else if (equals(command, "FLOOD"))
  {    
    for (int flower = 0; flower < NUM_FLOWERS; flower++)
      deflate(flower);

    for (int i = 0; i < NUM_SERIAL; i++)
      sendSerialData(i, "FLOOD");

    delay(numArgs == 1 ? args[0][0] : 10000);

    for (int flower = 0; flower < NUM_FLOWERS; flower++)
    {
      inflate(flower);
    }

    //wait  a minute and then set to automode again
    delay(60000);
    autoMode = true;
    return true;
  }
  else if (equals(command, "SET_COLOR"))
  {
    int red = args[0][0];
    int green = args[0][1];
    int blue = args[0][2];
    // Set whole tile if no arguments were given
    if (numArgs == 1)
      for (int flower = 0; flower < NUM_FLOWERS; flower++)
        setColor(flower, red, green, blue);
    // Otherwise loop through arguments, each time checking number of subarguments
    for (int i = 1; i < numArgs; i++)
    {
      switch (numSubArgs(i))
      {
        case 0: break;
        case 1: setColor(args[i][0], red, green, blue); break; // Set whole flower if one subarg was given
        case 2: // Set given led on each strip of given flower if two subargs were given
          for (int strip = 0; strip < LED_STRIPS_PER_FLOWER; strip++)
            setColor(args[i][0], strip, args[i][1], red, green, blue);
          break;
        case 3: setColor(args[i][0], args[i][1], args[i][2], red, green, blue); break; // Set given led
      }
    }
    return true;
  }
  else if (equals(command, "INFLATE"))
  {
    // Inflate all flowers if no arguments were given
    if (numArgs == 0)
      for (int flower = 0; flower < NUM_FLOWERS; flower++)
        inflate(flower);
    // Otherwise loop through arguments, each time checking number of subarguments
    for (int i = 0; i < numArgs; i++)
    {
      switch (numSubArgs(i))
      {
        case 1: inflate(args[i][0]); break; // Inflate whole flower if one subarg was given
        case 2: inflate(args[i][0], args[i][1]); break; // Inflate given pouch of given flower if two subargs were given
      }
    }
    return true;
  }
  else if (equals(command, "DEFLATE"))
  {
    // Deflate all flowers if no arguments were given
    if (numArgs == 0)
      for (int flower = 0; flower < NUM_FLOWERS; flower++)
        deflate(flower);
    // Otherwise loop through arguments, each time checking number of subarguments
    for (int i = 0; i < numArgs; i++)
    {
      switch (numSubArgs(i))
      {
        case 1: deflate(args[i][0]); break; // Deflate whole flower if one subarg was given
        case 2: deflate(args[i][0], args[i][1]); break; // Deflate given pouch of given flower if two subargs were given
      }
    }
    return true;
  }
  else if (equals(command, "INFLATE_TIME"))
  {
    // Set valve time for all flowers if no flowers specified
    if (numArgs == 1 && numSubArgs(0) == 1 && args[0][0] > 0) // Only one argument given, and that argument has only one number (which is positive)
      for (int flower = 0; flower < NUM_FLOWERS; flower++)
      {
        debugPrint("Setting valve time for "); debugPrint(flower / 2); debugPrint(" to "); debugPrintln(args[0][0]);
        valveTiming[flower / 2] = args[0][0];
      }
    // Set valve time for each flower specified
    for (int i = 0; i < numArgs; i++)
      if (args[i][1] > 0) // Time must be positive
      {
        debugPrint("Setting valve time for "); debugPrint(args[i][0] / 2); debugPrint(" to "); debugPrintln(args[i][1]);
        valveTiming[args[i][0] / 2] = args[i][1];
      }
    return true;
  }
  else if (equals(command, "KIOSK"))
  {
    bool terminate = false;
    byte curColor[3] = {0, 0, 0};
    setColor(curColor[0], curColor[1], curColor[2]);
    bool targetReached = true;
    int delayTime = 100;
    unsigned long lastUpdateTime = 0;
    byte targetColor[3] = {0,0,0};
    while(!terminate)
    {
      // Choose a random target color if the last target was reached
      if(targetReached)
      {
        for(int i = 0; i < 3; i++)
          targetColor[i] = random(255);
        targetReached = false;
      }
      // Change current color to be a little closer to target color
      if(millis() - lastUpdateTime > delayTime)
      {
        targetReached = true;
        for(int i = 0; i < 3; i++)
        {
          if(curColor[i] != targetColor[i])
          {
            targetReached = false;
            curColor[i] += (targetColor[i]-curColor[i]) > 0 ? 1 : -1;
          }
        }
        setColor(curColor[0], curColor[1], curColor[2]);
        lastUpdateTime = millis();
      }
      // See if we should terminate
      terminate = Serial.available() || Serial3.available() || !digitalRead(modeSwitchMasterPin);
      for(int serialNum = 0; serialNum < NUM_SERIAL; serialNum++)
        if(serial[serialNum].available())
          terminate = true;
    }
  }
  else
  {
    debugPrintln("Unrecognized command");
    autoMode = oldAutoMode;
    return false;
  }
  return true;
}

//----------------------
// Parses given data into command and arguments
// Returns whether or not command was valid
//----------------------
bool parseData(char* data, int dataLength)
{
  // Clear previous command and args
  command[0] = '\0';
  numArgs = 0;
  for (int i = 0; i < NUM_ARGS; i++)
    for (int s = 0; s < NUM_SUBARGS; s++)
      args[i][s] = -1;
  // If no data to parse, return
  if (dataLength == 0)
    return false;
  debugPrintln("Parsing data");

  //Get start of command (which is after target tile address)
  commandStartIndex = indexOf(data, "_") + 1;

  // Get start of first argument
  int argStart = indexOf(data, "<");
  argStart = argStart >= 0 ? argStart : length(data);
  int index = 0;
  int i = 0;
  //Get target tile address
  for (; i < commandStartIndex; i++)
  {
    tileNumberHolder[i] = data[i];
    index++;
  }
  tileNumberHolder[i] = '\0';

  tileNumber = commandStartIndex > 0 ? atoi(tileNumberHolder) : -1;

  int j = 0;
  // Get command name
  for (; j < argStart - commandStartIndex; j++)
  {
    command[j] = data[index];
    index++;
  }
  command[j] = '\0';
  // Get arguments
  int argNum = 0;
  int subArgNum = 0;
  char arg[6];
  int argIndex = 0;
  while (index < dataLength)
  {
    if (data[index] == '\0')
    {
      dataLength = index + 1;
      index++;
      continue;
    }
    if (data[index] == '<')
      index++;
    arg[argIndex++] = data[index];
    if (data[index] != '-' && data[index] != '>' && data[index] != '\0' && (data[index] > '9' || data[index] < '0'))
    {
      debugPrint("Bad data <"); debugPrint(data[index]);
      debugPrintln("> - aborting command");
      command[0] = '\0';
      numArgs = 0;
      return false;
    }
    index++;
    // See if a new arg or subArg is ending
    if (data[index] == '-')
    {
      arg[argIndex] = '\0';
      args[argNum][subArgNum] = (int) atof(arg);
      argIndex = 0;
      subArgNum++;
      index++;
    }
    else if (data[index] == '>')
    {
      arg[argIndex] = '\0';
      args[argNum][subArgNum] = (int) atof(arg);
      argIndex = 0;
      subArgNum = 0;
      argNum++;
      index++;
    }
  }
  numArgs = argNum;
  Serial.print("\nGot command name <"); Serial.print(command); Serial.print(">"); Serial.print(" and "); Serial.print(numArgs); Serial.println(" args");
  Serial.print("  From "); Serial.print(serialSender); Serial.print(" for tile "); Serial.print(tileNumber);
  Serial.print(" at time "); Serial.println(millis());
  return true;
}

//================================================
// Serial printing functions
//================================================

void debugPrint(const char* message)
{
#ifdef DEBUG
  Serial.print(message);
#endif
}

void debugPrintln(const char* message)
{
#ifdef DEBUG
  Serial.println(message);
#endif
}

void debugPrint(double message)
{
#ifdef DEBUG
  Serial.print(message);
#endif
}

void debugPrintln(double message)
{
#ifdef DEBUG
  Serial.println(message);
#endif
}

void debugPrint(int message)
{
#ifdef DEBUG
  Serial.print(message);
#endif
}

void debugPrintln(int message)
{
#ifdef DEBUG
  Serial.println(message);
#endif
}

void debugPrint(unsigned long message)
{
#ifdef DEBUG
  Serial.print(message);
#endif
}

void debugPrintln(unsigned long message)
{
#ifdef DEBUG
  Serial.println(message);
#endif
}

void debugPrint(char message)
{
#ifdef DEBUG
  Serial.print((char)message);
#endif
}

void debugPrintln(char message)
{
#ifdef DEBUG
  Serial.println((char)message);
#endif
}

void debugPrintln()
{
#ifdef DEBUG
  Serial.println();
#endif
}


//================================================
// Graph Coloring
//================================================
void initColoring()
{
  color = -1;
  colorDelay = 0;
  for (int i = 0; i < numColorNeighbors; i++)
    colors[i] = -1;
  for (int i = 0; i < NUM_FLOWERS; i++)
  {
    setColor(i, 0, 0, 0);
    deflate(i);
  }
  coloringInitialized = true;
}

void processColoring(int sender, int colors[5])
{
  delay(colorDelay);
  sender = sender >= 0 ? sender : LEFT;
  for (int i = 0; i < 5; i++)
  {
    if (colors[i] > 10)
      colors[i] = -1;
  }
  updateColors(sender, colors);
  if (color < 0)
    chooseColor();
  if (serialPresent[RIGHT])
  {
    // Send command along to the right tile
    sendColors(RIGHT);
    for (int i = 0; i < NUM_FLOWERS; i++)
    {
      setColor(i, (((color + 1) >> 0) % 2) * 150,
               (((color + 1) >> 1) % 2) * 150,
               (((color + 1) >> 2) % 2) * 150);
    }
    // Wait for acknowledgement
    while (!getSerialData(RIGHT))
    {
      delay(10);
    }
    // Update our known colors
    parseData(serialData, serialDataLength);
    colors[0] = args[0][0];
    colors[1] = args[0][1];
    colors[2] = args[1][0];
    colors[3] = args[2][0];
    colors[4] = args[2][1];
    updateColors(RIGHT, colors);
  }
  if (serialPresent[BOTTOM])
  {
    // Send command alongto bottom tile
    sendColors(BOTTOM);
    for (int i = 0; i < NUM_FLOWERS; i++)
    {
      setColor(i, (((color + 1) >> 0) % 2) * 150,
               (((color + 1) >> 1) % 2) * 150,
               (((color + 1) >> 2) % 2) * 150);
    }
    // Wait for acknowledgement
    while (!getSerialData(BOTTOM))
    {
      delay(10);
    }
    // Update our known colors
    parseData(serialData, serialDataLength);
    colors[0] = args[0][0];
    colors[1] = args[0][1];
    colors[2] = args[1][0];
    colors[3] = args[2][0];
    colors[4] = args[2][1];
    updateColors(BOTTOM, colors);
  }
  // Send known colors back as acknowledgement
  sendColors(sender);
  for (int i = 0; i < NUM_FLOWERS; i++)
  {
    setColor(i, (((color + 1) >> 0) % 2) * 150,
             (((color + 1) >> 1) % 2) * 150,
             (((color + 1) >> 2) % 2) * 150);
  }
}

void updateColors(int sender, int newColors[5])
{
  switch (sender)
  {
    case LEFT: // I am getting BOTTOM, BL, LEFT, TL, TOP
      colors[BOTTOM] = newColors[0] >= 0 ? newColors[0] : colors[BOTTOM];
      colors[BL] = newColors[1] >= 0 ? newColors[1] : colors[BL];
      colors[LEFT] = newColors[2] >= 0 ? newColors[2] : colors[LEFT];
      colors[TL] = newColors[3] >= 0 ? newColors[3] : colors[TL];
      colors[TOP] = newColors[4] >= 0 ? newColors[4] : colors[TOP];
      break;
    case RIGHT: // I am getting BOTTOM, BR, RIGHT, TR, TOP
      colors[BOTTOM] = newColors[0] >= 0 ? newColors[0] : colors[BOTTOM];
      colors[BR] = newColors[1] >= 0 ? newColors[1] : colors[BR];
      colors[RIGHT] = newColors[2] >= 0 ? newColors[2] : colors[RIGHT];
      colors[TR] = newColors[3] >= 0 ? newColors[3] : colors[TR];
      colors[TOP] = newColors[4] >= 0 ? newColors[4] : colors[TOP];
      break;
    case TOP: // I am getting LEFT, TL, TOP, TR, RIGHT
      colors[LEFT] = newColors[0] >= 0 ? newColors[0] : colors[LEFT];
      colors[TL] = newColors[1] >= 0 ? newColors[1] : colors[TL];
      colors[TOP] = newColors[2] >= 0 ? newColors[2] : colors[TOP];
      colors[TR] = newColors[3] >= 0 ? newColors[3] : colors[TR];
      colors[RIGHT] = newColors[4] >= 0 ? newColors[4] : colors[RIGHT];
      break;
    case BOTTOM: // I am getting LEFT, BL, BOTTOM, BR, RIGHT
      colors[LEFT] = newColors[0] >= 0 ? newColors[0] : colors[LEFT];
      colors[BL] = newColors[1] >= 0 ? newColors[1] : colors[BL];
      colors[BOTTOM] = newColors[2] >= 0 ? newColors[2] : colors[BOTTOM];
      colors[BR] = newColors[3] >= 0 ? newColors[3] : colors[BR];
      colors[RIGHT] = newColors[4] >= 0 ? newColors[4] : colors[RIGHT];
      break;
  }
  debugPrintln("See colors: ");
  debugPrint(colors[TL]); debugPrint("  ");
  debugPrint(colors[TOP]); debugPrint("  ");
  debugPrint(colors[TR]); debugPrint("  ");

  debugPrint(colors[LEFT]); debugPrint("  ");
  debugPrint(color); debugPrint("  ");
  debugPrint(colors[RIGHT]); debugPrint("  ");

  debugPrint(colors[BL]); debugPrint("  ");
  debugPrint(colors[BOTTOM]); debugPrint("  ");
  debugPrint(colors[BR]); debugPrint("  ");
}

void chooseColor()
{
  boolean foundColor = false;
  color = 0;
  while (!foundColor)
  {
    foundColor = true;
    for (int i = 0; i < numColorNeighbors; i++)
    {
      if (colors[i] == color)
      {
        color++;
        foundColor = false;
      }
    }
  }
  Serial.print("*** I chose color "); Serial.println(color);
}

void sendColors(int dest)
{
  switch (dest)
  {
    case RIGHT:
      sendColors(dest, colors[BR], colors[BOTTOM], color, colors[TOP], colors[TR]);
      break;
    case BOTTOM:
      sendColors(dest, colors[BL], colors[LEFT], color, colors[RIGHT], colors[BR]);
      break;
    case TOP:
      sendColors(dest, colors[TL], colors[LEFT], color, colors[RIGHT], colors[TR]);
      break;
    case LEFT:
      sendColors(dest, colors[BL], colors[BOTTOM], color, colors[TOP], colors[TL]);
      break;
  }
}

void sendColors(int dest, int color0, int color1, int color2, int color3, int color4)
{
  char toSend[40];
  toSend[0] = '\0';
  concat(toSend, "COLOR<", toSend, 40);
  concatInt(toSend, color0 == -1 ? 20 : color0, toSend, 40);
  concat(toSend, "-", toSend, 40);
  concatInt(toSend, color1 == -1 ? 20 : color1, toSend, 40);
  concat(toSend, ">", toSend, 40);

  concat(toSend, "<", toSend, 40);
  concatInt(toSend, color2 == -1 ? 20 : color2, toSend, 40);
  concat(toSend, ">", toSend, 40);

  concat(toSend, "<", toSend, 40);
  concatInt(toSend, color3 == -1 ? 20 : color3, toSend, 40);
  concat(toSend, "-", toSend, 40);
  concatInt(toSend, color4 == -1 ? 20 : color4, toSend, 40);
  concat(toSend, ">", toSend, 40);

  if (colorDelay > 0)
  {
    concat(toSend, "<", toSend, 40);
    concatInt(toSend, colorDelay, toSend, 40);
    concat(toSend, ">", toSend, 40);
  }

  sendSerialData(dest, toSend);
}


#endif

