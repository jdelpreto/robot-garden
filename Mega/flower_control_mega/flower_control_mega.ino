  /**
    A program for controlling up to 9 flowers
    Each flower has 3 pins: 1 of them can be used as an analog input, and any of them can be used as general digital I/O or for an RGB LED strip
    There are also 3 valve pins per flower which can be used as general digital I/O
      Each valve will only stay open for a predefined maximum time (set the valveTiming array in flower_control_mega.h)
    The following functions are useful (see flower_control_mega.h for full list)
      -- void deflate(int flower);   // Deflates all valves on the flower
      -- void inflate(int flower);   // Inflates all valves on the flower
      -- void deflate(int flower, int valve);  // Deflates the given valve
      -- void inflate(int flower, int valve);  // Inflates the given valve
      -- void setColor(int flower, byte red, byte green, byte blue);    // Sets the color of all LEDs on the flower (all strips)
      -- void setColor(int flower, int strip, byte red, byte green, byte blue);   // Sets the color of all LEDs on the strip of the flower
      -- void setColor(int flower, int strip, int led, byte red, byte green, byte blue);    // Sets the color of the individual LED
  **/
  
  //#define DEBUG // Must be defined before including the flower_control_mega.h file
  #include "flower_control_mega.h"  
  #include "OneWireSerial.h"
  #include "string_functions.h"
    
  void setup() 
  {
    //Serial.begin(9600);
    Serial.println("Starting setup...");
 
    setupFlowerControl();

    // Use the built-in LED on the Arduinos for debugging
    pinMode(13, OUTPUT);
    Serial.println("\tcomplete!");
//    
//    // Blink to confirm setup completed
//    for(int i = 0; i < 5; i++)
//    {
//      digitalWrite(13, HIGH);
//      delay(50);
//      digitalWrite(13, LOW);
//      delay(100);
//    }

    // Wait for everyone's program to start
    delay(5000);
    Serial.print("Done waiting, starting pinging at time "); Serial.println(millis());
    
    // Send pings to neighbors
    sendSerialData(RIGHT, "ping");
    sendSerialData(BOTTOM, "ping");
    sendSerialData(TOP, "ping");
    sendSerialData(LEFT, "ping");
    // Wait a little for responses
    long startWait = millis();
    while(millis() - startWait < 2000)
    {
      processSerial();
      delay(100);
    }
    Serial.print("Done waiting for ping responses, starting real program at time "); Serial.println(millis());
  }
  
  void loop() 
  {
    autoAddress(); // If not set, will request neighbor's addresses.  When set, will blink LED tileAddress+1 times
    processSerial();
    processBluetooth();
    processCOM();
    processModeSwitches();
    controlValves();    
  }
  
  // Delays for the given number of milliseconds, or until autoMode is turned off (whichever comes first)
  // Polls bluetooth while delaying
  // Will delay in 500ms increments so this should not be used for precise timing
  void autoModeDelay(unsigned long ms)
  {
    unsigned long start = millis();
    while(millis() - start < ms && autoMode)
    {
      processBluetooth(); // Has a 500ms timeout
    }
  }
  
  

  
  
