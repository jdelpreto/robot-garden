@echo off
setlocal enabledelayedexpansion
echo.

set arduinoPath="C:\Program Files (x86)\Arduino\arduino.exe"

set boardArg=mega
set sketchArg=flower_control_mega.ino
set portNumArg=%1
set portNumArg=%portNumArg:"=%

IF not [%2]==[] set sketchArg=%2
IF not [%3]==[] set boardArg=%3
@echo. >_tempTiles.txt
IF not [%1]==[] (
  for %%a in (!portNumArg!) do (
    @echo %%a >>_tempTiles.txt
  )
) else (
  for /F %%a in (tiles.txt) do (
    @echo %%a >>_tempTiles.txt
  )
)

IF !boardArg!==promini set boardArg=avr:pro:cpu=8MHzatmega328
IF !boardArg!==mega set boardArg=arduino:avr:mega:cpu=atmega2560

set tileNum=0
for /F %%a in (comPorts.txt) do (
set ports[!tileNum!]=%%a
set /a tileNum=!tileNum!+1
)

if exist tempBuild rmdir tempBuild /S /Q
REM echo Building the sketch...
REM mkdir tempBuild
REM !arduinoPath! --pref build.path=%CD%\tempBuild --board !boardArg! --verify %CD%\!sketchArg!

for /F %%a in (_tempTiles.txt) do (
  set portNum=!ports[%%a]!
  REM echo %%a: !portNum!
  IF x!portNum!x==xx (
    echo *** No COM port known for tile address "%%a" ***
  ) else (
    echo Programming tile %%a...
    REM echo !boardArg! COM!portNum! !sketchArg!
    !arduinoPath! --pref editor.external=true --pref build.path=%CD%\tempBuild --board !boardArg! --port COM!portNum! --upload %CD%\!sketchArg!
  )
)

if exist _tempTiles.txt del _tempTiles.txt
if exist tempBuild rmdir tempBuild /S /Q

echo.
echo All Done^^!
pause