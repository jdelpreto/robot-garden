
Before using programTiles:

   -- Make sure Arduino version 1.5.8 is installed 
      Download from http://arduino.cc/en/main/software#toc3

   -- Put programTiles.bat, tiles.txt, and comPorts.txt in the same folder as flower_control_mega.ino

   -- Open programTiles.bat in a text editor
      Make sure the line 
        set arduinoPath="C:\Program Files (x86)\Arduino\arduino.exe"
      points to where you installed the Arduino IDE

   -- Open comPorts.txt and enter the COM port numbers of each tile
      The first line is the port number of tile 0, the second of tile 1, etc.
      This is found by plugging in a tile, opening the Arduino IDE, and seeing which COM port is listed for it
        (or by opening Device Manager, expanding "Ports (COM & LPT)", and looking for your Arduino)

To use programTiles:
   
    Option (1)
      -- Enter the indices of the tiles you want programmed in tiles.txt
      -- Run programTiles.bat with no arguments (from command line or just double-click it)
      -- It will program the tiles specified in tiles.txt
      -- It will use flower_control_mega.ino in the same folder as programTiles.bat

    Option (2)
      -- Run programTiles.bat from the command line, specifying the desired tiles as an argument
      -- This is done by providing a string of tile indices, separated by spaces and enclosed in quotes
      -- For example, providing "0 1 2" as an argument (with the quotes) will program only tiles 0, 1, and 2
      -- The desired sketch can be provided as an optional second argument
      -- If the second argument is omitted, it will look to flower_control_mega.ino in the same folder as programTiles.bat
      
    Examples:
      -- double-click programTiles.bat
         This will program the tiles specified in tiles.txt with flower_control_mega.ino

      -- run the following from the command line:
            programTiles.bat "0 3 10"
         This will program tiles 0, 3, and 10 with flower_control_mega.ino

      -- run the following from the command line:
            programTiles.bat "0 3 10" mySketch.ino
         This will program tiles 0, 3, and 10 with mySketch.ino (in the same folder as programTiles.bat)

      -- run the following from the command line:
	    programTiles.bat "0 3 10" mySketch.ino promini
         This will program tiles 0, 3, and 10 with mySketch.ino (in the same folder as programTiles.bat) 
         and assume that all tiles use an Arduino ProMini instead of a Mega


