from GardenService import GardenService
from BluetoothService import severOldConnections
import tkinter
from tkinter import *
import tkinter.font as tkFont
import time
import math
from math import *

from ColorWheel import ColorWheel

class Garden:
    garden = None
    
    def __init__(self, tilesWide=6, tilesHigh=3, tileWidth=150, tileHeight=150, enablePrint=True, flowersPerTile=8, stripsPerFlower=3, ledsPerStrip=5):
        # Set up garden service
        self._garden = GardenService(numTiles=16, enablePrint=enablePrint, \
                                     flowersPerTile=flowersPerTile, stripsPerFlower=stripsPerFlower, ledsPerStrip=ledsPerStrip)
        self._numTiles = (tilesWide, tilesHigh)
        self._tileSize = (tileWidth, tileHeight)
        garden = self._garden
        garden.setAddress(0, '20:14:05:20:13:44')
        #garden.setAddress(5, '20:14:05:21:32:25')
        #garden.setAddress(9, '20:14:04:29:28:23')
        #garden.setAddress(15, '20:14:04:24:14:12')
        #garden.setAddress(3, '20:14:04:29:22:88')
        #garden.setAddress(4, '20:13:12:02:20:29')

        #garden.setAddress(0, '20:14:05:20:13:44')
        #garden.setAddress(1, '20:13:12:02:18:92')
        #garden.setAddress(2, '20:14:05:19:32:73')
        
        """
        garden.setAddress(0, '20:14:05:20:13:44')
        garden.setAddress(1, '20:13:12:02:18:92')
        garden.setAddress(2, '20:14:05:19:32:73')
        garden.setAddress(3, '20:14:04:29:22:88')
        garden.setAddress(4, '20:13:12:02:20:29')
        garden.setAddress(5, '20:14:05:21:32:25')
        garden.setAddress(6, '20:14:04:24:12:95')
        garden.setAddress(7, '20:14:05:09:19:68')
        garden.setAddress(8, '20:14:04:24:11:95')
        garden.setAddress(9, '20:14:04:29:28:23')
        garden.setAddress(10, '20:14:05:09:20:45')
        garden.setAddress(11, '20:14:05:21:29:71')
        garden.setAddress(12, '20:14:05:05:31:78')
        garden.setAddress(13, '20:14:04:29:16:80')
        garden.setAddress(14, '20:14:04:29:36:41')
        garden.setAddress(15, '20:14:04:24:14:12')
        """
        garden.connectTiles()
        
        
        # Set up GUI
        self._top = tkinter.Tk()
        canvasWidth = (self._numTiles[0]*self._tileSize[0])+200
        canvasHeight = (self._numTiles[1]*self._tileSize[1])+200
        self._top.geometry(str('%dx%d' % (canvasWidth+250, canvasHeight)))
        self._leftFrame = Frame(self._top)
        self._bottomFrame = Frame(self._top)
        self._controlsFrame = Frame(self._leftFrame) 
        self._canvasFrame = makeCanvas(self._top, canvasWidth, canvasHeight)
        
        self._canvas = getCanvas(self._canvasFrame)
        Widget.bind(self._canvas, "<ButtonPress-1>", self._OnCanvasPress)
        
        self._leftFrame.pack(side='left')
        self._controlsFrame.pack()
        self._bottomFrame.pack(side='bottom')
        self._canvasFrame.pack(anchor=E)

        self._selected = []
        self._curMotionItem = -1
        self._chosenBrightness = IntVar()
        self._chosenBrightness.set(100)
        self._chosenRGB = (1,1,1)
        self._selectedTiles = []
        self._selectedFlowers = []
        self._codeTileQueue = []
        self._codeFlowerQueue = []
        self._codeCommandQueue = []

        self._controlOption = StringVar(self._top)
        self._controlOption.set("Select Flower Control")


        #Add option buttons
        self._controlByClick = tkinter.Button(self._controlsFrame, command = self._controlByClickInterface, text='Control by Click')
        self._controlByCode = tkinter.Button(self._controlsFrame, command = self._controlByCodeInterface, text = 'Control by Code')

        self._controlByClick.grid(row=0, column=0, padx=5, pady=5)
        self._controlByCode.grid(row=0, column=1, padx=5, pady=5)

        
        self._drawGarden()
        time.clock() # start clock (useful on Windows)

        #self._redButton = tkinter.Button(self._controlsFrame, command=self._selectRed, text='Red')

        # Close connections on exit
        self._top.protocol("WM_DELETE_WINDOW", self._exit)

    def _exit(self):
        self._garden.disconnectTiles()
        self._top.quit()
        self._top.destroy()
        
    def _getTileTag(self, tileNum):
        return 'tileNum_' + str(tileNum)

    def _getTileNum(self, item):
        tags = self._canvas.gettags(item)
        for tag in tags:
            if tag.find("tileNum_") >= 0:
                return int(tag[tag.find("tileNum_") + len("tileNum_") :])
        return -1

    def _getFlowerTag(self, flowerNum):
        return 'flowerNum_' + str(flowerNum)

    def _getFlowerNum(self, item):
        tags = self._canvas.gettags(item)
        for tag in tags:
            if tag.find("flowerNum_") >= 0:
                return int(tag[tag.find("flowerNum_") + len("flowerNum_") :])
        return -1

    def _isFlower(self, item):
        return (self._getFlowerNum(item) >= 0)
    
    def _drawGarden(self):
        """
        Draws the garden on the canvas
        Number of tiles is given by self._numTiles, and tile size is given by self._tileSize
        """

        
        canvas = self._canvas
        origin = (50,50)
        tileSize = self._tileSize
        numTiles = self._numTiles[0]*self._numTiles[1]

        
        flowerPositions = ((20, 15), # Define position of each flower in a tile. \
                           (25, 40), # Each number is a percent of tile size.\
                           (15, 70), # Note that x increases to the right and y increases downward.\
                           (55, 25), # (origin is upper left corner).\
                           (45, 60), \
                           (85, 20), \
                           (70, 50), \
                           (75, 80))
        """
        flowerPositions = ((20, 20), # Define position of each flower in a tile. \
                           (20, 20), # Each number is a percent of tile size.\
                           (20, 20), # Note that x increases to the right and y increases downward.\
                           (20, 20), # (origin is upper left corner).\
                           (20, 20), \
                           (20, 20), \
                           (20, 20), \
                           (20, 20))
        tileRotations = [0, 0, 0, 0, 0, 0, 0, 0]
        """
        flowersPerTile = len(flowerPositions)
        flowerSize = min((tileSize[0] / (flowersPerTile/1.5), tileSize[1] / (flowersPerTile/1.5))) # Diameter of flower circle
        haveFlowers = [[True for i in range(flowersPerTile)] for j in range(numTiles)]  # If a flower is not plugged in, set its value to False in this array and it won't be drawn
        # Turn off flowers in pond area
        haveFlowers[8] = [False]*flowersPerTile
        haveFlowers[9] = [False]*flowersPerTile
        # Create tile grid and flowers in each tile
        self._tileBoxes = [-1]*numTiles # List of canvas item IDs - each is a box representing a tile
        self._flowers = [[-1 for i in range(flowersPerTile)] for j in range(numTiles)] # 2D list of canvas item IDs - each is a circle representing a flower
        for tileY in range(self._numTiles[1]):
            for tileX in range(self._numTiles[0]):
                tile = tileX + (tileY*self._numTiles[0])
                tileOrigin = (origin[0] + tileX*tileSize[0], origin[1] + tileY*tileSize[1])
                # Create tile box
                self._tileBoxes[tile] = canvas.create_rectangle(tileOrigin[0], tileOrigin[1], \
                        tileOrigin[0]+tileSize[0], tileOrigin[1]+tileSize[1], fill='white', activefill='orange', \
                        tags = ('tile', self._getTileTag(tile)))
                
                # Create flower circles
                lastFlowerOrigin = 0
                for flower in range(flowersPerTile):
                    if not haveFlowers[tile][flower]:
                        continue
                    flowerPos = flowerPositions[flower]
                    flowerOrigin = (tileOrigin[0] + int(flowerPos[0]*tileSize[0]/100), tileOrigin[1] + int(flowerPos[1]*tileSize[1]/100))
                    self._flowers[tile][flower] = canvas.create_oval(flowerOrigin[0] - flowerSize/2, flowerOrigin[1] - flowerSize/2, \
                                                    flowerOrigin[0] + flowerSize/2, flowerOrigin[1] + flowerSize/2, \
                                                    fill='white', activeoutline='orange', \
                                                    tags = ('flower', self._getFlowerTag(flower), self._getTileTag(tile)))
                    """
                    if lastFlowerOrigin != 0:
                        addedX = 1.5*math.cos(math.atan((lastFlowerOrigin[1]-flowerOrigin[1])/(lastFlowerOrigin[0]-flowerOrigin[0])))
                        addedY = 1.5*math.sin(math.atan((lastFlowerOrigin[1]-flowerOrigin[1])/(lastFlowerOrigin[0]-flowerOrigin[0])))
                        canvas.create_line((flowerOrigin[0] + addedX), (flowerOrigin[1] + addedY), (lastFlowerOrigin[0] - addedX), (lastFlowerOrigin[1] - addedY), fill = 'red')
                    lastFlowerOrigin = flowerOrigin
                    """
                """
                # Bind all flowers to the flower click event
                self._canvas.tag_bind('flower', "<ButtonPress-1>", self._OnFlowerPress)
                self._canvas.tag_bind('flower', "<ButtonRelease-1>", self._OnFlowerRelease)
                self._canvas.tag_bind('flower', "<B1-Motion>", self._OnFlowerMotion)
                # Bind the tile box to the tile click event 
                self._canvas.tag_bind('tile', "<ButtonPress-1>", self._OnTilePress)
                self._canvas.tag_bind('tile', "<ButtonRelease-1>", self._OnTileRelease)
                self._canvas.tag_bind('tile', "<B1-Motion>", self._OnTileMotion)
                """

    def _refreshFlowerControls(self):
        print('REFRESH CONTROLS')
        items = self._selected[:]
        self._controlsFrame.destroy()
        self._controlsFrame = Frame(self._leftFrame)
        self._controlsFrame.pack()
        '''
        if len(items) == 0:
            return
        '''
        plural = 's' if len(items) > 1 else ''
        # Create button for opening and closing flowers
        #actuationLabel = Label(self._controlsFrame, text='Flower Motion', font=tkFont.Font(size=13))
        actuationLabel = Label(self._controlsFrame, text='Control by Click', font=tkFont.Font(size=13))
        openButton = tkinter.Button(self._controlsFrame, command=self._openFlowers, text='Open Flower' + plural)
        closeButton = tkinter.Button(self._controlsFrame, command=self._closeFlowers, text='Close Flower' + plural)

        distributedAlgorithmsLabel = Label(self._controlsFrame, text = 'Distributed Algorithms', font=tkFont.Font(size=10))

        # Create button for flood algorithm
        floodButton = tkinter.Button(self._controlsFrame, command = self._flood, text = "Flood")

        # Create button for graph coloring algorithm
        graphColoringButton = tkinter.Button(self._controlsFrame, command = self._graphColoring, text = "Graph Coloring")


        # Create color wheel for choosing color
        colorWheelLabel = Label(self._controlsFrame, text='Flower Color', font=tkFont.Font(size=13))
        colorWheel = ColorWheel(self._controlsFrame, immediate=0, width=200, height=200, stripes=100, circles=20)
        self._chosenRGB = (1,1,1)
        colorWheel.AddCallback(lambda color:self._OnChooseColor(color=color))
        self._chosenBrightness.set(100)
        brightnessSlider = Scale(self._controlsFrame, from_=0, to=255, resolution=5, showvalue=0, length=200, \
                                 orient=HORIZONTAL, variable=self._chosenBrightness)
        brightnessSlider.bind("<ButtonRelease-1>", self._OnChooseColor)
        # Create buttons for turning LEDs on and off
        offButton = tkinter.Button(self._controlsFrame, command=self._turnOff, text='Turn Off')
        onButton = tkinter.Button(self._controlsFrame, command=self._turnOn, text='Turn On')

        
        
        actuationLabel.grid(row=0, column=0, columnspan=2, padx=5, pady=(40,5))
        openButton.grid(row = 1, column = 0, columnspan=2, padx=5, pady=5)
        closeButton.grid(row = 2, column = 0, columnspan=2, padx=5, pady=5)
        colorWheelLabel.grid(row=3, column=0, columnspan=2, padx=5,pady=(40,5))
        colorWheel.getCanvas().grid(row=4, column=0, columnspan=2, padx=5, pady=5)
        brightnessSlider.grid(row=6, column=0, columnspan=2, padx=5, pady=0)
        offButton.grid(row=7, column=0, padx=5, pady=5)
        onButton.grid(row=7, column=1, padx=5, pady=5)
        distributedAlgorithmsLabel.grid(row=9, column = 0, columnspan=2, padx=5, pady=(40,5))
        floodButton.grid(row=10, column=0, padx=5, pady=5)
        graphColoringButton.grid(row=10, column=1, padx=5, pady=5)

    def _controlByCodeInterface(self):
        #print('This is where the code interface will go')
        self._controlByClick.destroy()
        self._controlByCode.destroy()


        #Title for Control by Code Page
        titleLabel = Label(self._controlsFrame, text='Control by Code', font=tkFont.Font(size=13))
        titleLabel.grid(row=0, column=0, columnspan=2, padx=5, pady=(40,5))

        #Conditional Logic Label and Buttons
        '''
        conditionalLogicLabel = Label(self._controlsFrame, text='Conditional Logic', font=tkFont.Font(size=9))
        conditionalLogicLabel.grid(row=1,column=0)
        ifButton = tkinter.Button(self._controlsFrame, text = '   if   ').grid(row=2, column=0, padx=5, pady=5)
        elseButton = tkinter.Button(self._controlsFrame, text = 'else').grid(row=3, column=0, padx=5, pady=5)
        '''

        #Loop Label and Buttons
        '''
        loopLabel = Label(self._controlsFrame, text='Loops', font=tkFont.Font(size=9))
        loopLabel.grid(row=1,column=1)
        forButton = tkinter.Button(self._controlsFrame, text = '   for   ').grid(row=2, column=1, padx=5, pady=5)
        whileButton = tkinter.Button(self._controlsFrame, text = 'while').grid(row=3, column=1, padx=5, pady=5)
        '''

        #Add the box for code here
        #codeBox = LabelFrame(self._controlsFrame, width=250, height=250, bg = 'white')
        #codeBox.grid(row=10, column=0)
        
        #Run Code Button
        runCodeButton = tkinter.Button(self._controlsFrame, command = self._runCode, text='Run Code')
        runCodeButton.grid(row=12, column=0)

        #Try to add code to the code box
        self._code = Text(self._controlsFrame, font=tkFont.Font(size=8), width=50)
        #self._code.insert(INSERT,'main(){ \n ')
        #self._code.mark_set("}", INSERT)
        #self._code.insert(INSERT, "}")
        self._code.grid(row=10, column=0)

        #Add an "Add to Code" Button
        addToCodeButton = tkinter.Button(self._controlsFrame, command = self._addCodeToText, text='Add Selection to Code').grid(row=11, column = 0)

        #Add Control Button
        #flowerControlLabel = Label(self._controlsFrame, text='Flower Controls', font=tkFont.Font(size=9)).grid(row=1, column=0)


        #Make an Option Menu for flower control options
        flowerControlOptionMenu = OptionMenu(self._controlsFrame, self._controlOption, 'Red', 'Orange', 'Yellow', 'Green', 'Blue', 'Purple', 'Pink', 'White', 'Inflate', 'Deflate')
        flowerControlOptionMenu.grid(row=2, column=0)
        
        # Bind all flowers to the flower click event
        self._canvas.tag_bind('flower', "<ButtonPress-1>", self._OnFlowerPress)
        self._canvas.tag_bind('flower', "<ButtonRelease-1>", self._selectFlower)
        self._canvas.tag_bind('flower', "<B1-Motion>", self._OnFlowerMotion)
        # Bind the tile box to the tile click event 
        self._canvas.tag_bind('tile', "<ButtonPress-1>", self._OnTilePress)
        self._canvas.tag_bind('tile', "<ButtonRelease-1>", self._selectTile)
        self._canvas.tag_bind('tile', "<B1-Motion>", self._OnTileMotion)



        

    def _controlByClickInterface(self):
        #print('yo')
        self._controlByClick.destroy()
        self._controlByCode.destroy()
        self._refreshFlowerControls()

        # Bind all flowers to the flower click event
        self._canvas.tag_bind('flower', "<ButtonPress-1>", self._OnFlowerPress)
        self._canvas.tag_bind('flower', "<ButtonRelease-1>", self._OnFlowerRelease)
        self._canvas.tag_bind('flower', "<B1-Motion>", self._OnFlowerMotion)
        # Bind the tile box to the tile click event 
        self._canvas.tag_bind('tile', "<ButtonPress-1>", self._OnTilePress)
        self._canvas.tag_bind('tile', "<ButtonRelease-1>", self._OnTileRelease)
        self._canvas.tag_bind('tile', "<B1-Motion>", self._OnTileMotion)

    def _addCodeToText(self):
        
        if (self._controlOption.get()== 'Select Flower Control') or (len(self._selected)==0):
            print('Please Select Options')
        else:
            #self._code.delete("}", END)
            tileCount = 0
            flowerCount = 0
            
            #Separate tiles and flowers into separate lists
            for i in range(len(self._selected)):
                # or 19 or 28 or 37 or 46 or 55 or 64 or 75 or 84 or 93 or 102 or 111 or 120 or 129 or 138
                if (self._selected[i]<73):
                    if((self._selected[i]-1)%9 == 0):
                        self._selectedTiles.append(self._selected[i])
                        tileCount+=1
                    else:
                        self._selectedFlowers.append(self._selected[i])
                        flowerCount+=1
                else:
                    if((self._selected[i]-3)%9 == 0):
                        self._selectedTiles.append(self._selected[i])
                        tileCount+=1
                    else:
                        self._selectedFlowers.append(self._selected[i])
                        flowerCount+=1

            #Add appropriate text to text box
            self._code.insert(INSERT, "Make ")
            if (tileCount==0):
                if (flowerCount == 1):
                    self._code.insert(INSERT, "flower ")
                    self._code.insert(INSERT, str(self._getFlowerNum(self._selectedFlowers[0])) + " on tile " + str(self._getTileNum(self._selectedFlowers[0])) + " ")
                else:
                    #self._code.insert(INSERT, "flowers ")
                    for i in range(len(self._selectedFlowers)):
                        self._code.insert(INSERT, "flower " + str(self._getFlowerNum(self._selectedFlowers[i])) + " on tile " + str(self._getTileNum(self._selectedFlowers[i])))
                        if (i == len(self._selectedFlowers)-1):
                            self._code.insert(INSERT, " ")
                        elif (i == len(self._selectedFlowers)-2):
                            self._code.insert(INSERT, " and ")
                        else:
                            self._code.insert(INSERT, ", ")
                self._code.insert(INSERT, self._controlOption.get() + ";" + "\n")

            elif (tileCount ==1):
                self._code.insert(INSERT, "tile ")
                self._code.insert(INSERT, str(self._getTileNum(self._selectedTiles[0])) + " ")

                if (flowerCount == 0):
                    self._code.insert(INSERT, self._controlOption.get() + ";" + "\n")
                elif (flowerCount == 1):
                    self._code.insert(INSERT, "and flower " + str(self._getFlowerNum(self._selectedFlowers[0])) + " on tile " + str(self._getTileNum(self._selectedFlowers[i])))
                    self._code.insert(INSERT, self._controlOption.get() + ";" + "\n")
                else:
                    self._code.insert(INSERT, "and ")
                    for i in range(len(self._selectedFlowers)):
                        self._code.insert(INSERT, "flower " + str(self._getFlowerNum(self._selectedFlowers[i])) + " on tile " + str(self._getTileNum(self._selectedFlowers[i])))
                        if (i == len(self._selectedFlowers)-1):
                            self._code.insert(INSERT, " ")
                        elif (i == len(self._selectedFlowers)-2):
                            self._code.insert(INSERT, " and ")
                        else:
                            self._code.insert(INSERT, ", ")
                    self._code.insert(INSERT, self._controlOption.get() + ";" + "\n")
                    
                
            if (tileCount>1):
                self._code.insert(INSERT, "tiles ")
                for i in range(len(self._selectedTiles)):
                    self._code.insert(INSERT, str(self._getTileNum(self._selectedTiles[i])))
                    if (i == len(self._selectedTiles)-1):
                        self._code.insert(INSERT, " ")
                    elif(i == len(self._selectedTiles)-2):
                        self._code.insert(INSERT, " and ")
                    else:
                        self._code.insert(INSERT, ", ")
                if (flowerCount == 0):
                    self._code.insert(INSERT, self._controlOption.get() + ";" + "\n")
                elif (flowerCount == 1):
                    self._code.insert(INSERT, "and flower " + str(self._getFlowerNum(self._selectedFlowers[0])) + " on tile " + str(self._getTileNum(self._selectedFlowers[0])))
                    self._code.insert(INSERT, self._controlOption.get() + ";" + "\n")
                else:
                    self._code.insert(INSERT, "and ")
                    for i in range(len(self._selectedFlowers)):
                        self._code.insert(INSERT, "flower " + str(self._getFlowerNum(self._selectedFlowers[i])) + " on tile " + str(self._getTileNum(self._selectedFlowers[i])))
                        if (i == len(self._selectedFlowers)-1):
                            self._code.insert(INSERT, " ")
                        elif (i == len(self._selectedFlowers)-2):
                            self._code.insert(INSERT, " and ")
                        else:
                            self._code.insert(INSERT, ", ")
                    self._code.insert(INSERT, self._controlOption.get() + ";" + "\n")
                    
            '''                      
            print("Selected tiles and flowers: ")
            for i in range(len(self._selected)):
                 print(str(self._selected[i]) + ", ")
            print("\n Selected Control: " + self._controlOption.get())
            '''
            #self._code.insert(INSERT, "}")

        self._codeFlowerQueue.append(self._selectedFlowers[:])
        self._codeTileQueue.append(self._selectedTiles[:])
        self._codeCommandQueue.append(self._controlOption.get())

        print("Flower Queue: " + str(self._codeFlowerQueue))
        print("Tile Queue: " + str(self._codeTileQueue))
        print("Command Queue: " + str(self._codeCommandQueue))

        while len(self._selectedTiles)>0:
            self._selectedTiles.pop()
        while len(self._selectedFlowers)>0:
            self._selectedFlowers.pop()


    def _runCode(self):
        for i in range(len(self._codeCommandQueue)):
            codeCommand = self._codeCommandQueue[i]
            flowers = [[] for j in range(self._numTiles[0]*self._numTiles[1])]
            tiles = []
            # Sort by flower / tile
            selectedFlowers = []

            # Add tile numbers to tile list
            for k in range(len(self._codeTileQueue[i])):
                tileNum = self._getTileNum(self._codeTileQueue[i][k])
                tiles.append(tileNum)

            #Add flowers to flower list
            for k in range(len(self._codeFlowerQueue[i])):
                tileNum = self._getTileNum(self._codeFlowerQueue[i][k])
                if tileNum == 8 or tileNum ==9:
                    continue
                tileNum = tileNum
                flowers[tileNum].append(self._getFlowerNum(self._codeFlowerQueue[i][k]))
            
        
            # Send commands to individual flowers
            for tile in range(len(flowers)):
                if len(flowers[tile]) == 0:
                    continue
                if codeCommand == 'Red':
                    self._garden.setColor(tileNum=tile, flower=flowers[tile], color=(150,0,0))
                elif codeCommand == 'Orange':
                    self._garden.setColor(tileNum=tile, flower=flowers[tile], color=(150,75,0))
                elif codeCommand == 'Yellow':
                    self._garden.setColor(tileNum=tile, flower=flowers[tile], color=color(150,150,0))
                elif codeCommand == 'Green':
                    self._garden.setColor(tileNum=tile, flower=flowers[tile], color=(0,150,0))
                elif codeCommand == 'Blue':
                    self._garden.setColor(tileNum=tile, flower=flowers[tile], color=(0,0,150))
                elif codeCommand == 'Purple':
                    self._garden.setColor(tileNum=tile, flower=flowers[tile], color=(91,28,150))
                elif codeCommand == 'Pink':
                    self._garden.setColor(tileNum=tile, flower=flowers[tile], color=(140,11,81))
                elif codeCommand == 'White':
                    self._garden.setColor(tileNum=tile, flower=flowers[tile], color=(150,150,150))
                elif codeCommand == 'Deflate':
                    self._garden.deflate(tileNum=tile, flower=flowers[tile])
                elif codeCommand == 'Inflate':
                    self._garden.inflate(tileNum=tile, flower=flowers[tile])
            # Send commands to whole tiles
            for tile in tiles:
                if codeCommand == 'Red':
                    self._garden.setColor(tileNum=tile, color=(150,0,0))
                elif codeCommand == 'Orange':
                    self._garden.setColor(tileNum=tile, color=(150,75,0))
                elif codeCommand == 'Yellow':
                    self._garden.setColor(tileNum=tile, color=(150,150,0))
                elif codeCommand == 'Green':
                    self._garden.setColor(tileNum=tile, color=(0,150,0))
                elif codeCommand == 'Blue':
                    self._garden.setColor(tileNum=tile, color=(0,0,150))
                elif codeCommand == 'Purple':
                    self._garden.setColor(tileNum=tile, color=(91,28,150))
                elif codeCommand == 'Pink':
                    self._garden.setColor(tileNum=tile, color=(140,11,81))
                elif codeCommand == 'White':
                    self._garden.setColor(tileNum=tile, color=(150,150,150))
                elif codeCommand == 'Deflate':
                    self._garden.deflate(tileNum=tile)
                elif codeCommand == 'Inflate':
                    self._garden.inflate(tileNum=tile)

            time.sleep(3)
            
        
            
        
        
        
    def _select(self, item):
        self._selected.append(item)
        if self._isFlower(item):
            self._canvas.itemconfig(item, outline='red', activeoutline='red')
        else:
            self._canvas.itemconfig(item, fill='red', activeoutline='red')
        for item in self._selected:
            print ("Items Selected: " + str(self._selected))

    def _deselect(self, item):
        if item in self._selected:
            self._selected.remove(item)
        if self._isFlower(item):
            self._canvas.itemconfig(item, outline='black', activeoutline='orange')
        else:
            self._canvas.itemconfig(item, fill='white', activeoutline='orange')

    def _clearSelection(self):
        for item in self._selected[:]:
            self._deselect(item)
        self._curMotionItem = -1
            
    def _isSelected(self, item):
        return (item in self._selected)
        
    def _OnChooseColor(self, event=None, color=None):
        if color is not None:
            color = list(color[0:3])
            self._chosenRGB = color[:]
        else:
            color = list(self._chosenRGB[:])
        brightness = self._chosenBrightness.get()
        color[0] = int(color[0] * brightness)
        color[1] = int(color[1] * brightness)
        color[2] = int(color[2] * brightness)
        self._setColor(color)
        
    def _OnFlowerPress(self, event):
        canvas = self._canvas
        item = canvas.find_closest(event.x, event.y)[0]
        tags = canvas.gettags(item)
        print('PRESS FLOWER ' + str(item) + ': ', tags)
        tileNum = self._getTileNum(item)
        flowerNum = self._getFlowerNum(item)

    def _OnFlowerRelease(self, event):
        if self._curMotionItem >= 0:
            self._refreshFlowerControls()
            self._curMotionItem = -1
            return
        canvas = self._canvas
        item = canvas.find_closest(event.x, event.y)[0]
        tags = canvas.gettags(item)
        print('RELEASE FLOWER ' + str(item) + ': ', tags)
        tileNum = self._getTileNum(item)
        flowerNum = self._getFlowerNum(item)
        if self._isSelected(item):
            self._deselect(item)
        else:
            if not(tileNum == 8 or tileNum == 9): # Don't select pond tiles
                self._select(item)
        self._refreshFlowerControls()

    def _selectFlower(self, event):
        if self._curMotionItem >= 0:
            self._refreshFlowerControls()
            self._curMotionItem = -1
            return
        canvas = self._canvas
        item = canvas.find_closest(event.x, event.y)[0]
        tags = canvas.gettags(item)
        print('RELEASE FLOWER ' + str(item) + ': ', tags)
        tileNum = self._getTileNum(item)
        flowerNum = self._getFlowerNum(item)
        if self._isSelected(item):
            self._deselect(item)
        else:
            if not(tileNum == 8 or tileNum == 9): # Don't select pond tiles
                self._select(item)
        
    def _OnFlowerMotion(self, event):
        canvas = self._canvas
        item = canvas.find_closest(event.x, event.y)[0]
        tags = canvas.gettags(item)
        
        tileNum = self._getTileNum(item)
        flowerNum = self._getFlowerNum(item)
        if not self._isFlower(item):
            return
        coords = list(canvas.coords(item))
        if event.x < coords[0] or event.x > coords[2]:
            return
        if event.y < coords[1] or event.y > coords[3]:
            return
        if self._curMotionItem == item:
            return
        if self._curMotionItem >= 0 and not self._isFlower(self._curMotionItem):
            return
        print('MOTION FLOWER ' + str(item) + ': ', tags)
        if self._isSelected(item):
            self._deselect(item)
        else:
            if not(tileNum == 8 or tileNum == 9): # Don't select pond tiles
                self._select(item)
        self._curMotionItem = item
        print('curmotionitem is ', self._curMotionItem)
        
    def _OnTilePress(self, event):
        canvas = self._canvas
        item = canvas.find_closest(event.x, event.y)[0]
        tags = canvas.gettags(item)
        print('PRESS TILE ' + str(item) + ': ', tags)
        tileNum = self._getTileNum(item)

    def _OnTileRelease(self, event):
        if self._curMotionItem >= 0:
            self._refreshFlowerControls()
            self._curMotionItem = -1
            return
        canvas = self._canvas
        item = canvas.find_closest(event.x, event.y)[0]
        tags = canvas.gettags(item)
        print('RELEASE TILE ' + str(item) + ': ', tags)
        tileNum = self._getTileNum(item)
        if self._isSelected(item):
            self._deselect(item)
        else:
            if not(tileNum == 8 or tileNum == 9): # Don't select pond tiles
                self._select(item)
        self._refreshFlowerControls()

    def _selectTile(self, event):
        if self._curMotionItem >= 0:
            self._refreshFlowerControls()
            self._curMotionItem = -1
            return
        canvas = self._canvas
        item = canvas.find_closest(event.x, event.y)[0]
        tags = canvas.gettags(item)
        print('RELEASE TILE ' + str(item) + ': ', tags)
        tileNum = self._getTileNum(item)
        if self._isSelected(item):
            self._deselect(item)
        else:
            if not(tileNum == 8 or tileNum == 9): # Don't select pond tiles
                self._select(item)

    def _OnCanvasPress(self, event):
        if event.widget.find_withtag(CURRENT):
            return
        canvas = self._canvas
        self._clearSelection()
        self._refreshFlowerControls()
        
    def _OnTileMotion(self, event):
        canvas = self._canvas
        item = canvas.find_closest(event.x, event.y)[0]
        tags = canvas.gettags(item)
        
        tileNum = self._getTileNum(item)
        flowerNum = self._getFlowerNum(item)
        if self._isFlower(item):
            return
        coords = list(canvas.coords(item))
        if event.x < coords[0] or event.x > coords[2]:
            return
        if event.y < coords[1] or event.y > coords[3]:
            return
        if self._curMotionItem == item:
            return
        if self._curMotionItem >= 0 and self._isFlower(self._curMotionItem):
            return
        print('MOTION TILE ' + str(item) + ': ', tags)
        if self._isSelected(item):
            self._deselect(item)
        else:
            if not(tileNum == 8 or tileNum == 9): # Don't select pond tiles
                self._select(item)
        self._curMotionItem = item

    def _flood(self):
        self._garden.flood(waitForAck=True)

    def _graphColoring(self):
        self._garden.graphColoring(waitForAck=True)

    def _openFlowers(self):
        self._sendCommand(command='DEFLATE')

    def _closeFlowers(self):
        self._sendCommand(command='INFLATE')

    def _setColor(self, color):
        self._sendCommand(command='COLOR', color=color)

    def _turnOn(self):
        self._OnChooseColor(self._chosenRGB)

    def _turnOff(self):
        self._setColor(color=(0,0,0))


    # TODO check the logic in this function, I'm not too positive about it
    def _sendCommand(self, command=None, color=None):
        if command is None:
            return
        flowers = [[] for j in range(self._numTiles[0]*self._numTiles[1])]
        tiles = []
        # Sort by flower / tile
        selectedFlowers = []
        for selected in self._selected[:]:
            if self._isFlower(selected):
                selectedFlowers.append(selected)
            else:
                tileNum = self._getTileNum(selected)
                if tileNum == 8 or tileNum == 9:
                    continue
                # Commenting out the part where tile numbering is fixed
                '''if tileNum < 8 else tileNum-2'''
                tiles.append(tileNum)
        # Group flowers by tile
        for flower in selectedFlowers:
            tileNum = self._getTileNum(flower)
            if tileNum == 8 or tileNum == 9:
                continue
            # Again, commenting out the part where tile numbering is fixed
            '''if tileNum < 8 else tileNum - 2'''
            tileNum = tileNum
            flowers[tileNum].append(self._getFlowerNum(flower))        
        # Send commands to individual flowers
        for tile in range(len(flowers)):
            if len(flowers[tile]) == 0:
                continue
            if command == 'COLOR':
                self._garden.setColor(tileNum=tile, flower=flowers[tile], color=color)
            elif command == 'DEFLATE':
                self._garden.deflate(tileNum=tile, flower=flowers[tile])
            elif command == 'INFLATE':
                self._garden.inflate(tileNum=tile, flower=flowers[tile])
        # Send commands to whole tiles
        for tile in tiles:
            if command == 'COLOR':
                self._garden.setColor(tileNum=tile, color=color)
            elif command == 'DEFLATE':
                self._garden.deflate(tileNum=tile)
            elif command == 'INFLATE':
                self._garden.inflate(tileNum=tile)
        
        
    def start(self):
        self._top.mainloop()


def makeCanvas(top, canvasWidth=-1, canvasHeight=-1):
    frame = Frame(top, bd=2, relief=SUNKEN)
    
    frame.grid_rowconfigure(0, weight=1)
    frame.grid_columnconfigure(0, weight=1)
    
    xscrollbar = Scrollbar(frame, orient=HORIZONTAL)
    xscrollbar.grid(row=1, column=0, sticky=E+W)
    
    yscrollbar = Scrollbar(frame)
    yscrollbar.grid(row=0, column=1, sticky=N+S)
    
    canvas = Canvas(frame, bd=0, bg='white',
                    xscrollcommand=xscrollbar.set,
                    yscrollcommand=yscrollbar.set)
    
    canvas.grid(row=0, column=0, sticky=N+S+E+W)
    canvas.config(scrollregion=canvas.bbox(ALL))
    if canvasWidth > 0:
        canvas.config(width = canvasWidth)
    if canvasHeight > 0:
        canvas.config(height = canvasHeight)
    if canvasWidth > 0 and canvasHeight > 0:
        canvas.config(scrollregion=(0, 0, canvasWidth, canvasHeight))
    
    xscrollbar.config(command=canvas.xview)
    yscrollbar.config(command=canvas.yview)
    
    return frame

def getCanvas(widget):
    if isinstance(widget, Canvas):
        return widget
    else:
        for child in widget.winfo_children():
            if isinstance(child, Canvas):
                return child
    return None

"""
Some test code...
"""
if __name__ == '__main__':
    garden = Garden()
    garden.start()
    










