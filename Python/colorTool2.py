def OneColor(color): 
  """get a color RGBA Add alpha if missing, return values from 0.0 to 1.0""" 

  if max(color) > 1.0: 
      color = map( lambda x: x/255., color) 
  if len(color) == 3: 
      return list(color)+[1.] 
  elif len(color)==4: 
      return color 
  else: 
      raise ValueError( 'Color has to be a 3 or 4 tuple (RGBA)' ) 


def TkColor(col): 
  """col should be a rgb triplet of int 0-255 or 0-1"""
  if max(col)<=1.0: col = map( lambda x: x*255, col)
  col=list(col)
  return '#%02X%02X%02X' % (col[0],col[1],col[2]) 


def ToRGB(hsv): 
  """Convert an HSV triplet into an RGB triplet. 
  Values range from 0.0 to 1.0. Alpha values are optional""" 

  l = len(hsv) 
  assert l in (3,4) 
  assert max(hsv) <= 1.0 
  assert min(hsv) >= 0.0 
  v = hsv[2] 
  if v == 0.0: 
      if l==3: return (0.0, 0.0, 0.0) 
      else: return (0.0, 0.0, 0.0,hsv[3]) 
  s = hsv[1] 
  if s == 0.0: 
      if l==3: return (v, v, v) 
      else: return (v, v, v,hsv[3]) 
  h = hsv[0]*6.0 
  if h>=6.0: h = 0.0 
  i = int(h) 
  f = h - i 
  p = v*(1.0 - s) 
  q = v*(1.0-(s*f)) 
  t = v*(1.0-s*(1.0-f)) 

  if i==0: 
      if l==3: return (v,t,p) 
      else: return (v,t,p,hsv[3]) 
  elif i==1: 
      if l==3: return (q,v,p) 
      else: return (q,v,p,hsv[3]) 
  elif i==2: 
      if l==3: return (p,v,t) 
      else: return (p,v,t,hsv[3]) 
  elif i==3: 
      if l==3: return (p,q,v) 
      else: return (p,q,v,hsv[3]) 
  elif i==4: 
      if l==3: return (t,p,v) 
      else: return (t,p,v,hsv[3]) 
  elif i==5: 
      if l==3: return (v,p,q) 
      else: return (v,p,q,hsv[3]) 
  else: 
      print("botch in hsv_to_rgb" )


def ToHSV(rgb): 
  """Convert an RGB triplet into an HSV triplet. 
     Values range from 0.0 to 1.0. Alpha values are optional""" 
  l = len(rgb) 
  assert l in (3,4) 
  assert max(rgb) <= 1.0 
  assert min(rgb) >= 0.0 
  r = rgb[0]; 
  g = rgb[1]; 
  b = rgb[2]; 
  maxi = max(rgb[:3]) 
  mini = min(rgb[:3]) 

  if maxi > 0.0001: s = (maxi - mini)/maxi 
  else: s = 0.0 
  if s < 0.0001: h = 0.0 
  else: 
      delta = maxi - mini 
      if r == maxi: h = (g - b)/delta 
      elif g == maxi: h = 2.0 + (b - r)/delta 
      elif b == maxi: h = 4.0 + (r - g)/delta 
      h = h/6.0 
      if h < 0.0: h = h + 1.0 

  if l==3: return (h,s,maxi) 
  else: return (h,s,maxi,rgb[3])
